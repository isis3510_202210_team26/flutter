import 'package:flutter/material.dart';

const COLOR_PINK = Color.fromRGBO(246, 65, 108, 1);
const COLOR_BLUE = Color.fromRGBO(0, 187, 169, 1);
const COLOR_YELLOW = Color.fromRGBO(255, 211, 97, 1);
const COLOR_BLACK = Color.fromRGBO(55, 65, 64, 1);

const TextTheme TEXT_THEME_DEFAULT = TextTheme(
  headline1:
      TextStyle(color: COLOR_BLACK, fontWeight: FontWeight.w600, fontSize: 20),
  headline2:
      TextStyle(color: COLOR_BLACK, fontWeight: FontWeight.w600, fontSize: 16),
  headline3:
      TextStyle(color: COLOR_BLACK, fontWeight: FontWeight.w500, fontSize: 14),
  headline4:
      TextStyle(color: COLOR_BLACK, fontWeight: FontWeight.w500, fontSize: 14),
  headline5:
      TextStyle(color: COLOR_BLACK, fontWeight: FontWeight.w400, fontSize: 14),
);
