import 'package:amplify_auth_cognito/amplify_auth_cognito.dart';
import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:flutter/material.dart';
import 'package:gifter/screens/interests.dart';
import '../models/Tiempo.dart';
import './interests.dart';
import 'dart:async';
import 'package:connectivity_plus/connectivity_plus.dart';

import 'frequency.dart';

// ignore: camel_case_types
class emailpage extends StatefulWidget {
  final String emails;
  final String users;
  final String password;
  emailpage(
      {Key? key,
      required this.emails,
      required this.users,
      required this.password})
      : super(key: key);

  @override
  State<emailpage> createState() => _emailpageState();
}

class _emailpageState extends State<emailpage> {
  ConnectivityResult result = ConnectivityResult.none;
  late StreamSubscription subscription;
  final TextEditingController _username = TextEditingController();

  final TextEditingController _code = TextEditingController();
  @override
  double getTimeNow() {
    String now = DateTime.now().toString();
    String time = now.split(" ")[1];
    var arraytime = time.split(":");
    int minute = int.parse(arraytime[1]);
    int hour = int.parse(arraytime[0]);
    double seconds = double.parse(arraytime[2]);
    double timeSeconds = minute * 60 + seconds + hour * 60 * 60;
    return timeSeconds;
  }

  double inicio = 0;
  void initState() {
    super.initState();
    inicio = getTimeNow();
    // registerFrequency("Email Confirmation");
    Connectivity().checkConnectivity().then((value) {
      this.result = value;
    });
    subscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      setState(() => this.result = result);
      if (result == ConnectivityResult.none) {
        setState(() => _showSnackBar());
      } else {
        setState(() => ScaffoldMessenger.of(context).hideCurrentSnackBar());
      }
      // Got a new connectivity status!
    });
  }

  dispose() {
    super.dispose();

    subscription.cancel();
  }

  void _showSnackBar() {
    ScaffoldMessenger.of(context).hideCurrentSnackBar();

    SnackBar snackBar = const SnackBar(
        content: Text("There is no internet connection"),
        duration: Duration(days: 1));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.transparent,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back,
              color: Color.fromRGBO(246, 65, 108, 1)),
          onPressed: () async {
            double last = getTimeNow();
            double tiempoStay = last - inicio;
            try {
              final tiemporegister = Tiempo(name: "Email", time: tiempoStay);
              await Amplify.DataStore.save(tiemporegister);
            } catch (e) {
              print(e);
            }
            Navigator.of(context).pop();
          },
        ),
        title: RichText(
            text: const TextSpan(
                text: "Email confirmation",
                style: TextStyle(
                    color: Color.fromRGBO(246, 65, 108, 1),
                    fontWeight: FontWeight.w600,
                    fontSize: 17,
                    fontFamily: 'WorkSans'))),
        elevation: 0.0,
      ),
      body: SingleChildScrollView(
        child: SizedBox(
          width: size.width,
          height: size.height,
          child: Column(
            children: [
              SizedBox(
                height: size.height * (1 / 14.8),
              ),
              const Icon(
                Icons.email,
                size: 106,
                color: Color.fromRGBO(246, 65, 108, 1),
              ),
              RichText(
                  text: const TextSpan(
                      text: "We sent a confirmation code to",
                      style: TextStyle(
                          color: Color.fromRGBO(55, 65, 64, 1),
                          fontWeight: FontWeight.w600,
                          fontSize: 15,
                          fontFamily: 'WorkSans'))),
              RichText(
                  text: TextSpan(
                      text: widget.emails,
                      style: const TextStyle(
                          color: Color.fromRGBO(0, 187, 169, 1),
                          fontWeight: FontWeight.w600,
                          fontSize: 15,
                          fontFamily: 'WorkSans'))),
              SizedBox(
                height: size.height * (1 / 13),
              ),
              SizedBox(
                width: size.width - 50,
                height: 36,
                child: Material(
                  elevation: 3.0,
                  shadowColor: Colors.grey,
                  child: TextFormField(
                    autofocus: false,
                    controller: _username,
                    decoration: InputDecoration(
                        hintText: 'Username',
                        fillColor: Colors.white,
                        filled: true,
                        contentPadding:
                            const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20.0),
                            borderSide: const BorderSide(
                                color: Color.fromRGBO(255, 255, 255, 150)))),
                  ),
                  borderRadius: BorderRadius.circular(20.0),
                ),
              ),
              const SizedBox(
                height: 16,
              ),
              SizedBox(
                width: size.width - 50,
                height: size.height * (1 / 16.44),
                child: Material(
                  elevation: 3.0,
                  shadowColor: Colors.grey,
                  child: TextFormField(
                    obscureText: true,
                    autofocus: false,
                    controller: _code,
                    decoration: InputDecoration(
                        hintText: 'Insert code',
                        fillColor: Colors.white,
                        filled: true,
                        contentPadding:
                            const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20.0),
                            borderSide: const BorderSide(
                                color: Color.fromRGBO(255, 255, 255, 150)))),
                  ),
                  borderRadius: BorderRadius.circular(20.0),
                ),
              ),
              SizedBox(
                height: size.height * (1 / 12.33),
              ),
              MaterialButton(
                child: const Text('Confirm Email',
                    style: TextStyle(
                        fontSize: 14,
                        fontFamily: "WorkSans",
                        fontWeight: FontWeight.w700,
                        color: Color.fromRGBO(255, 255, 255, 1))),
                onPressed: () async {
                  double last = getTimeNow();
                  double tiempoStay = last - inicio;
                  try {
                    final tiemporegister =
                        Tiempo(name: "Email", time: tiempoStay);
                    await Amplify.DataStore.save(tiemporegister);
                  } catch (e) {
                    print(e);
                  }
                  _submitCode(context);
                },
                color: const Color.fromRGBO(246, 65, 108, 1),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0)),
                minWidth: size.width - 80.0,
                height: 40,
              ),
              const SizedBox(
                height: 40,
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _submitCode(BuildContext context) async {
    if (result == ConnectivityResult.none) {
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
            title: const Text("There is no network connection"),
            content: const Text(
                "Please check your network connection and try again, you can access this page once again by going to login with your username and password."),
            actions: <Widget>[
              FlatButton(
                child: const Text("Continue"),
                onPressed: Navigator.of(context).pop,
              )
            ]),
      );
      return;
    }
    final confirmationCode = _code.text;
    final usernamee = _username.text;
    if (usernamee != "" && confirmationCode != "") {
      try {
        var confirmSign = await Amplify.Auth.confirmSignUp(
            username: usernamee, confirmationCode: confirmationCode);
        if (confirmSign.isSignUpComplete) {
          await Amplify.Auth.signIn(
              username: usernamee, password: widget.password);
          try {
            // ignore: unused_local_variable
            final awsUser = await Amplify.Auth.getCurrentUser();
            //send user to dashboard
          } on AuthException catch (e) {
            //
            await Amplify.Auth.signOut();
          }
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (_) => interest(user: usernamee),
            ),
          );
          registerFrequency("Interests");
        }
      } on AuthException catch (e) {
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
              title: const Text("Wrong username or wrong code"),
              content: const Text("Please try again"),
              actions: <Widget>[
                // ignore: deprecated_member_use
                FlatButton(
                  child: const Text("Continue"),
                  onPressed: Navigator.of(context).pop,
                )
              ]),
        );
      }
    } else {
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
            title: const Text(
                "Please input the username and the confirmation code"),
            content: const Text(
                "The confirmation code should have been sent to your email, check the spam folder"),
            actions: <Widget>[
              // ignore: deprecated_member_use
              FlatButton(
                child: const Text("Continue"),
                onPressed: Navigator.of(context).pop,
              )
            ]),
      );
    }
  }
}
