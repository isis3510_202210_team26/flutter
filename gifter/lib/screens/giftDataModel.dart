// ignore_for_file: file_names

class GiftDataModel {
  String? id;
  String? name;
  String? description;
  String? image;
  List<String>? categories;
  double? price;
  // List<String>? stores;

  GiftDataModel({
    this.id,
    this.name,
    this.description,
    this.image,
    this.categories,
    this.price,
    // this.stores
  });

  GiftDataModel.fromJson(Map<String, dynamic> json) {
    image = json['image'];
    categories = List<String>.from(json['categories']);
    price = json['price'].toDouble();
    id = json['id'];
    description = json['description'];
    name = json['name'];
    // stores = List<String>.from(json['stores']);
  }
}
