import 'package:flutter/material.dart';

class Dialogs {
  static Future<void> showLoadingDialog(
      BuildContext context, GlobalKey key) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return WillPopScope(
              onWillPop: () async => false,
              child: SimpleDialog(
                  key: key,
                  backgroundColor: Colors.black54,
                  children: <Widget>[
                    Center(
                      // ignore: prefer_const_literals_to_create_immutables
                      child: Column(children: [
                        const CircularProgressIndicator(
                          color: Color.fromRGBO(246, 65, 108, 1),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        const Text(
                          "Please Wait....",
                          style:
                              TextStyle(color: Color.fromRGBO(246, 65, 108, 1)),
                        )
                      ]),
                    )
                  ]));
        });
  }
}
