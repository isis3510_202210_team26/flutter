// ignore_for_file: non_constant_identifier_names, must_be_immutable, file_names

import 'dart:convert';
import 'dart:isolate';

import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:gifter/models/ModelProvider.dart';
import 'package:gifter/screens/StoreDataModel.dart';
import 'package:gifter/screens/giftFeed.dart';
import 'package:gifter/screens/storeDetail.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'frequency.dart';

class GiftDetail extends StatefulWidget {
  String id;
  String name;
  String image;
  String description;
  List<StoreDataModel>? stores;
  double avgPrice;
  List<String> categories;
  bool pressed;

  GiftDetail(this.id, this.name, this.image, this.description, this.avgPrice,
      this.categories, this.pressed,
      {this.stores, Key? key})
      : super(key: key);

  @override
  State<GiftDetail> createState() => _GiftDetailState();
}

class _GiftDetailState extends State<GiftDetail> {
  String firstHalf = "";
  String secondHalf = "";
  bool flag = true;

  late int _selectedIndex;
  late List<Widget> _pages;

  bool isPressed = false;
  Color heartColor = Colors.white;

  late Size size;
  double getTimeNow() {
    String now = DateTime.now().toString();
    String time = now.split(" ")[1];
    var arraytime = time.split(":");
    int minute = int.parse(arraytime[1]);
    int hour = int.parse(arraytime[0]);
    double seconds = double.parse(arraytime[2]);
    double timeSeconds = minute * 60 + seconds + hour * 60 * 60;
    return timeSeconds;
  }

  var giftSt;

  double inicio = 0;
  @override
  void initState() {
    super.initState();
    bq();
    giftSt = giftStores();
    inicio = getTimeNow();
    // registerFrequency("Gift Detail");
    isPressed = widget.pressed;
    heartColor = widget.pressed ? Colors.redAccent : Colors.white;

    if (widget.description.length > 200) {
      firstHalf = widget.description.substring(0, 200);
      secondHalf = widget.description.substring(200, widget.description.length);
    } else {
      firstHalf = widget.description;
      secondHalf = "";
    }
  }

  Future<void> calculateTime(int ver) async {
    final prefs = await SharedPreferences.getInstance();
    var comienzo = prefs.getDouble("comenzar");
    var user = prefs.getString("user");
    var users = "";
    if (user != null) {
      users = user;
    }
    double last = getTimeNow();
    double tiempoFinal = last - comienzo!;
    try {
      final tiemporegister = HomeToProfile(username: users, time: tiempoFinal);
      await Amplify.DataStore.save(tiemporegister);
    } catch (e) {
      print(e);
    }
  }

  Future<void> bq() async {
    final prefs = await SharedPreferences.getInstance();
    var comienzo = prefs.getDouble("comenzar");
    var conteo = prefs.getString("conteo");
    if (conteo == "0") {
      prefs.setString("conteo", "1");
      var horafinal = getTimeNow();
      calculateTime(1);
    }
  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;

    return WillPopScope(
        onWillPop: () {
          Navigator.pop(context, false);
          registerFrequency("Feed");
          return Future.value(false);
        },
        child: SafeArea(
          child: Scaffold(
            body: ListView(
              scrollDirection: Axis.vertical,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    CoverImage,
                    GetCategories,
                    InfoPrice,
                    GetDivider,
                    Stores
                  ],
                )
              ],
            ),
            // bottomNavigationBar: buildNavigation(),
          ),
        ));
  }

  Widget get CoverImage {
    return Container(
        color: const Color.fromRGBO(224, 224, 224, 1),
        child: Stack(alignment: AlignmentDirectional.topCenter, children: [
          // Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
          CachedNetworkImage(
            imageUrl: widget.image,
            height: 250,
            fit: BoxFit.fitHeight,
            color: const Color.fromRGBO(224, 224, 224, 1),
            colorBlendMode: BlendMode.modulate,
            placeholder: (context, url) => Center(
                child: Transform.scale(
              scale: 0.5,
              child: const CircularProgressIndicator(),
            )),
            errorWidget: (context, url, error) =>
                const Center(child: Icon(Icons.error)),
          ),
          // Favorite button
          Align(
              alignment: Alignment.topRight,
              child: IconButton(
                splashRadius: 10,
                iconSize: 24,
                icon: Icon(Icons.favorite, color: heartColor),
                onPressed: () {
                  setState(() {
                    isPressed = !isPressed;
                    heartColor = isPressed ? Colors.redAccent : Colors.white;
                  });
                  saveToWishlist(isPressed, widget.id);
                },
              ))
        ]));
  }

  Widget get GetCategories {
    var categoriesList = widget.categories;
    return Padding(
        padding: const EdgeInsets.only(top: 16, bottom: 16, left: 16),
        child: SizedBox(
            height: 35,
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: categoriesList.length,
                itemBuilder: (context, index) {
                  return Padding(
                      padding: const EdgeInsets.only(right: 16),
                      child: MaterialButton(
                        elevation: 0.0,
                        child: Text(categoriesList[index],
                            style: const TextStyle(
                                fontSize: 14,
                                fontFamily: "WorkSans",
                                fontWeight: FontWeight.w700,
                                color: Colors.white)),
                        color: const Color.fromRGBO(0, 187, 169, 1),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                            side: const BorderSide(
                                width: 1.0,
                                color: Color.fromRGBO(0, 187, 169, 1))),
                        //height: 30,
                        onPressed: () {},
                      ));
                })));
  }

  Widget get InfoPrice {
    return Container(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Column(children: [
          Align(
              alignment: Alignment.centerLeft,
              child: Text(
                widget.name,
                style: const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w600,
                    fontFamily: "Montserrat"),
              )),
          const Padding(padding: EdgeInsets.only(bottom: 8)),
          Container(
              child: secondHalf.isEmpty
                  ? Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        widget.description.split(".")[0] + '.',
                        style: const TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                            fontFamily: "WorkSans"),
                      ))
                  : Column(
                      children: [
                        Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              flag
                                  ? (firstHalf + "...")
                                  : (firstHalf + secondHalf),
                              style: const TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "WorkSans"),
                            )),
                        const Padding(
                          padding: EdgeInsets.only(bottom: 4),
                        ),
                        Align(
                            alignment: Alignment.centerRight,
                            child: InkWell(
                                onTap: () {
                                  setState(() {
                                    flag = !flag;
                                  });
                                },
                                child: Text(
                                  flag
                                      ? "See full description"
                                      : "Hide full description",
                                  style: const TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w600,
                                      color: Color.fromRGBO(246, 65, 108, 1),
                                      fontFamily: "WorkSans"),
                                )))
                      ],
                    )),
          const Padding(padding: EdgeInsets.only(bottom: 8)),
          Align(
              alignment: Alignment.centerLeft,
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: Container(
                    color: const Color.fromRGBO(244, 244, 244, 1),
                    height: 31,
                    width: 78,
                    padding: const EdgeInsets.all(6),
                    alignment: Alignment.center,
                    child: FittedBox(
                        fit: BoxFit.fitWidth,
                        child: Text(
                          NumberFormat.currency(symbol: '\$', decimalDigits: 0)
                              .format(widget.avgPrice),
                          style: const TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.w400,
                              fontFamily: "Montserrat"),
                        )),
                  ))),
          const Padding(padding: EdgeInsets.only(bottom: 16)),
        ]));
  }

  Widget get GetDivider {
    return const Divider(
      color: Color.fromARGB(244, 230, 230, 230),
      thickness: 8,
    );
  }

  Widget get Stores {
    // List<StoreDataModel> st = widget.stores!;
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Column(children: [
        const Padding(padding: EdgeInsets.only(top: 16)),
        const Align(
          alignment: Alignment.centerLeft,
          child: Text(
            "Stores that sell this gift",
            style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w600,
                fontFamily: "Montserrat"),
          ),
        ),
        const Padding(padding: EdgeInsets.only(bottom: 16)),
        FutureBuilder(
            future: giftSt,
            builder: (context, data) {
              if (data.hasError) {
                return const SizedBox(
                    height: 100,
                    child: Align(
                        alignment: Alignment.center,
                        child: Text(
                          "There was an error trying to show the stores. Please check your connection and try again later.",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.blueGrey, fontFamily: "WorkSans"),
                        )));
              } else if (data.hasData) {
                var st = data.data as List<StoreDataModel>;
                if (st.isEmpty) {
                  return const SizedBox(
                    height: 50,
                    child: Center(
                        child: Text(
                      "There are no stores yet that sell this gift",
                      style: TextStyle(fontFamily: "Montserrat", fontSize: 16),
                    )),
                  );
                }
                return SizedBox(
                    height: 180,
                    child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: st.length,
                        itemBuilder: (context, index) {
                          var text = st[index].description!.length <= 40
                              ? st[index].description!
                              : st[index].description!.substring(0, 41) + "...";
                          return Padding(
                              padding: const EdgeInsets.only(right: 12),
                              child: InkWell(
                                  onTap: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (_) => StoreDetail(
                                                st[index].name,
                                                st[index].description,
                                                st[index].images,
                                                st[index].storeLoc,
                                                st[index].website,
                                                st[index].phone,
                                                st[index].address,
                                                // st[index].gifts,
                                                index)));
                                    registerFrequency("Store Detail");
                                  },
                                  child: SizedBox(
                                      width: 200,
                                      child: Card(
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(10.0),
                                        ),
                                        elevation: 5,
                                        child: Column(
                                          // mainAxisAlignment: MainAxisAlignment.start,
                                          // crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Expanded(
                                              child: ClipRRect(
                                                borderRadius:
                                                    const BorderRadius.only(
                                                        topLeft:
                                                            Radius.circular(10),
                                                        topRight:
                                                            Radius.circular(
                                                                10)),
                                                child: CachedNetworkImage(
                                                  width: 200,
                                                  imageUrl: st[index]
                                                      .images
                                                      .toString(),
                                                  fit: BoxFit.cover,
                                                  placeholder: (context, url) =>
                                                      Center(
                                                          child:
                                                              Transform.scale(
                                                    scale: 0.5,
                                                    child:
                                                        const CircularProgressIndicator(),
                                                  )),
                                                  errorWidget:
                                                      (context, url, error) =>
                                                          const Center(
                                                              child: Icon(
                                                                  Icons.error)),
                                                ),
                                              ),
                                            ),
                                            Container(
                                              padding: const EdgeInsets.all(8),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    st[index].name.toString(),
                                                    style: const TextStyle(
                                                        fontSize: 14,
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        fontFamily:
                                                            "Montserrat"),
                                                  ),
                                                  const Padding(
                                                      padding: EdgeInsets.only(
                                                          bottom: 4)),
                                                  SizedBox(
                                                      width: 200,
                                                      child: Text(text,
                                                          style: const TextStyle(
                                                              fontSize: 12,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w600,
                                                              fontFamily:
                                                                  "Montserrat",
                                                              color: Color
                                                                  .fromRGBO(
                                                                      115,
                                                                      135,
                                                                      133,
                                                                      1)))),
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                      ))));
                        }));
              } else {
                return Center(
                    child: Transform.scale(
                        scale: 0.5, child: const CircularProgressIndicator()));
              }
            }),
        const Padding(padding: EdgeInsets.only(bottom: 34)),
      ]),
    );
  }

  Future<List<StoreDataModel>> giftStores() async {
    Dio dio = Dio();
    final response = await dio.post(
        "https://us-central1-gifter-team26.cloudfunctions.net/getStoresSellingGift",
        data: jsonEncode({"giftId": widget.id}));
    if (response.statusCode == 200) {
      var stores = response.data as List<dynamic>;
      return stores.map((e) => StoreDataModel.fromJson(e)).toList();
      // return stores;
    } else {
      throw Exception("There was an error retrieving data");
    }
  }
}
