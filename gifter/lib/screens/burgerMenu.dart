import 'package:amplify_auth_cognito/amplify_auth_cognito.dart';
import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:flutter/material.dart';
import 'package:gifter/screens/frequency.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'firstScreen.dart';

class BurgerMenu extends StatefulWidget {
  const BurgerMenu({Key? key}) : super(key: key);

  @override
  State<BurgerMenu> createState() => _BurgerMenuState();
}

class _BurgerMenuState extends State<BurgerMenu> {
  Future _logout(BuildContext context) async {
    try {
      Navigator.of(context, rootNavigator: true).pushReplacement(
          MaterialPageRoute(builder: (context) => firstScreen()));
      // print(awsUser.username);
      await Amplify.Auth.signOut();
      final prefs = await SharedPreferences.getInstance();
      prefs.setString("online", "0");
      registerFrequency("First Screen");
      print("e");
      //send user to dashboard
    } on AuthException catch (e) {
      //

    }
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Column(
      children: [
        SizedBox(
          height: 100,
        ),
        Image(
          image: const AssetImage("assets/logo.png"),
          height: size.height * 0.1,
        ),
        SizedBox(
          height: 100,
        ),
        MaterialButton(
          child: const Text('Log Out',
              style: TextStyle(
                  fontSize: 14,
                  fontFamily: "WorkSans",
                  fontWeight: FontWeight.w700,
                  color: Color.fromRGBO(255, 255, 255, 1))),
          onPressed: () => _logout(context),
          color: const Color.fromRGBO(246, 65, 108, 1),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(18.0)),
          minWidth: 70,
          height: 26,
        ),
      ],
    );
  }
}
