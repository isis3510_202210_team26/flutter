// ignore_for_file: camel_case_types, must_be_immutable, unused_local_variable, deprecated_member_use

import 'package:amplify_auth_cognito/amplify_auth_cognito.dart';
import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gifter/screens/dialogs.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:amplify_api/amplify_api.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../models/Tiempo.dart';
import './email.dart';
import 'dart:async';
import '../models/User.dart';
import 'frequency.dart';

class signup extends StatefulWidget {
  signup({Key? key}) : super(key: key);

  @override
  State<signup> createState() => _signupState();
}

class _signupState extends State<signup> {
  ConnectivityResult result = ConnectivityResult.none;
  late StreamSubscription subscription;

  final GlobalKey<State> _keyLoader = GlobalKey<State>();

  bool revisando = false;

  final TextEditingController _name = TextEditingController();

  final TextEditingController _userController = TextEditingController();

  final TextEditingController _emailController = TextEditingController();

  final TextEditingController _passwordController = TextEditingController();

  final String _bio = "";

  final List<String> _interest = [];

  String _dates = "";

  int _age = 0;

  final String _profilePic = "";

  final String _coverImg = "";

  final List<String> friend = [];
  double getTimeNow() {
    String now = DateTime.now().toString();
    String time = now.split(" ")[1];
    var arraytime = time.split(":");
    int minute = int.parse(arraytime[1]);
    int hour = int.parse(arraytime[0]);
    double seconds = double.parse(arraytime[2]);
    double timeSeconds = minute * 60 + seconds + hour * 60 * 60;
    return timeSeconds;
  }

  double inicio = 0;
  @override
  void initState() {
    super.initState();
    inicio = getTimeNow();
    // registerFrequency("Sign Up");
    Connectivity().checkConnectivity().then((value) {
      this.result = value;
    });
    subscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      setState(() => this.result = result);
      if (result == ConnectivityResult.none) {
        setState(() => _showSnackBar());
      } else {
        setState(() => ScaffoldMessenger.of(context).hideCurrentSnackBar());
      }
      // Got a new connectivity status!
    });
  }

  dispose() {
    super.dispose();

    subscription.cancel();
  }

  void _showSnackBar() {
    ScaffoldMessenger.of(context).hideCurrentSnackBar();

    SnackBar snackBar = const SnackBar(
        content: Text("There is no internet connection"),
        duration: Duration(days: 1));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.transparent,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back,
              color: Color.fromRGBO(246, 65, 108, 1)),
          onPressed: () async {
            double last = getTimeNow();
            double tiempoStay = last - inicio;
            try {
              final tiemporegister = Tiempo(name: "Sign Up", time: tiempoStay);
              await Amplify.DataStore.save(tiemporegister);
            } catch (e) {
              print(e);
            }
            Navigator.of(context).pop();
            registerFrequency("First Screen");
          },
        ),
        title: RichText(
            text: const TextSpan(
                text: "Sign Up",
                style: TextStyle(
                    color: Color.fromRGBO(246, 65, 108, 1),
                    fontWeight: FontWeight.w600,
                    fontSize: 17,
                    fontFamily: 'WorkSans'))),
        elevation: 0.0,
      ),
      body: SingleChildScrollView(
        child: Form(
          child: SizedBox(
            width: size.width,
            height: size.height,
            child: Column(
              children: [
                SizedBox(
                  height: size.height * (1 / 29.6),
                ),
                Row(children: [
                  RichText(
                      text: const TextSpan(
                          text: "   What's your ",
                          style: TextStyle(
                              color: Color.fromRGBO(55, 65, 64, 1),
                              fontWeight: FontWeight.w600,
                              fontSize: 15,
                              fontFamily: 'WorkSans'))),
                  RichText(
                      text: const TextSpan(
                          text: "name",
                          style: TextStyle(
                              color: Color.fromRGBO(0, 187, 169, 1),
                              fontWeight: FontWeight.w600,
                              fontSize: 15,
                              fontFamily: 'WorkSans'))),
                  RichText(
                      text: const TextSpan(
                          text: "?",
                          style: TextStyle(
                              color: Color.fromRGBO(55, 65, 64, 1),
                              fontWeight: FontWeight.w400,
                              fontSize: 15,
                              fontFamily: 'WorkSans')))
                ]),
                SizedBox(
                  height: size.height * (1 / 37),
                ),
                SizedBox(
                  width: size.width - 50,
                  height: size.height * (1 / 18.5),
                  child: Material(
                    elevation: 2.0,
                    shadowColor: Colors.grey,
                    child: TextFormField(
                      autofocus: false,
                      controller: _name,
                      validator: (value) {
                        return (value == null
                            ? "There is nothing"
                            : (value.length < 2
                                ? 'Name must be greater than two characters'
                                : null));
                      },
                      inputFormatters: [
                        LengthLimitingTextInputFormatter(30),
                      ],
                      decoration: InputDecoration(
                          hintText: 'Name',
                          fillColor: Colors.white,
                          filled: true,
                          contentPadding:
                              const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(20.0),
                              borderSide: const BorderSide(
                                  color: Color.fromRGBO(255, 255, 255, 150)))),
                    ),
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                ),
                SizedBox(
                  height: size.height * (1 / 37),
                ),
                Row(children: [
                  RichText(
                      text: const TextSpan(
                          text: "   What's your ",
                          style: TextStyle(
                              color: Color.fromRGBO(55, 65, 64, 1),
                              fontWeight: FontWeight.w600,
                              fontSize: 15,
                              fontFamily: 'WorkSans'))),
                  RichText(
                      text: const TextSpan(
                          text: "email",
                          style: TextStyle(
                              color: Color.fromRGBO(0, 187, 169, 1),
                              fontWeight: FontWeight.w600,
                              fontSize: 15,
                              fontFamily: 'WorkSans'))),
                  RichText(
                      text: const TextSpan(
                          text: "?",
                          style: TextStyle(
                              color: Color.fromRGBO(55, 65, 64, 1),
                              fontWeight: FontWeight.w400,
                              fontSize: 15,
                              fontFamily: 'WorkSans')))
                ]),
                SizedBox(
                  height: size.height * (1 / 37),
                ),
                SizedBox(
                  width: size.width - 50,
                  height: size.height * (1 / 18.5),
                  child: Material(
                    elevation: 2.0,
                    shadowColor: Colors.grey,
                    child: TextFormField(
                      autofocus: false,
                      controller: _emailController,
                      validator: (value) {
                        return (value == null
                            ? "There is nothing"
                            : (value.length < 2
                                ? 'Name must be greater than two characters'
                                : null));
                      },
                      inputFormatters: [
                        LengthLimitingTextInputFormatter(50),
                      ],
                      decoration: InputDecoration(
                          hintText: 'Email',
                          fillColor: Colors.white,
                          filled: true,
                          contentPadding:
                              const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(20.0),
                              borderSide: const BorderSide(
                                  color: Color.fromRGBO(255, 255, 255, 150)))),
                    ),
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                ),
                SizedBox(
                  height: size.height * (1 / 37),
                ),
                Row(children: [
                  RichText(
                      text: const TextSpan(
                          text: "   Think of a cool ",
                          style: TextStyle(
                              color: Color.fromRGBO(55, 65, 64, 1),
                              fontWeight: FontWeight.w600,
                              fontSize: 15,
                              fontFamily: 'WorkSans'))),
                  RichText(
                      text: const TextSpan(
                          text: "username",
                          style: TextStyle(
                              color: Color.fromRGBO(0, 187, 169, 1),
                              fontWeight: FontWeight.w600,
                              fontSize: 15,
                              fontFamily: 'WorkSans'))),
                  RichText(
                      text: const TextSpan(
                          text: ".",
                          style: TextStyle(
                              color: Color.fromRGBO(55, 65, 64, 1),
                              fontWeight: FontWeight.w400,
                              fontSize: 15,
                              fontFamily: 'WorkSans')))
                ]),
                SizedBox(
                  height: size.height * (1 / 37),
                ),
                SizedBox(
                  width: size.width - 50,
                  height: size.height * (1 / 18.5),
                  child: Material(
                    elevation: 2.0,
                    shadowColor: Colors.grey,
                    child: TextFormField(
                      autofocus: false,
                      validator: (value) {
                        return (value == null
                            ? "There is nothing"
                            : (value.length < 2
                                ? 'Name must be greater than two characters'
                                : null));
                      },
                      inputFormatters: [
                        LengthLimitingTextInputFormatter(12),
                      ],
                      controller: _userController,
                      decoration: InputDecoration(
                          hintText: 'Username',
                          fillColor: Colors.white,
                          filled: true,
                          contentPadding:
                              const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(20.0),
                              borderSide: const BorderSide(
                                  color: Color.fromRGBO(255, 255, 255, 150)))),
                    ),
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                ),
                SizedBox(
                  height: size.height * (1 / 37),
                ),
                Row(children: [
                  RichText(
                      text: const TextSpan(
                          text: "   Set your ",
                          style: TextStyle(
                              color: Color.fromRGBO(55, 65, 64, 1),
                              fontWeight: FontWeight.w600,
                              fontSize: 15,
                              fontFamily: 'WorkSans'))),
                  RichText(
                      text: const TextSpan(
                          text: "password",
                          style: TextStyle(
                              color: Color.fromRGBO(0, 187, 169, 1),
                              fontWeight: FontWeight.w600,
                              fontSize: 15,
                              fontFamily: 'WorkSans'))),
                  RichText(
                      text: const TextSpan(
                          text: ".",
                          style: TextStyle(
                              color: Color.fromRGBO(55, 65, 64, 1),
                              fontWeight: FontWeight.w400,
                              fontSize: 15,
                              fontFamily: 'WorkSans')))
                ]),
                SizedBox(
                  height: size.height * (1 / 37),
                ),
                SizedBox(
                  width: size.width - 50,
                  height: size.height * (1 / 18.5),
                  child: Material(
                    elevation: 2.0,
                    shadowColor: Colors.grey,
                    child: TextFormField(
                      obscureText: true,
                      autofocus: false,
                      validator: (value) {
                        return (value == null
                            ? "There is nothing"
                            : (value.length < 2
                                ? 'Name must be greater than two characters'
                                : null));
                      },
                      inputFormatters: [
                        LengthLimitingTextInputFormatter(20),
                      ],
                      controller: _passwordController,
                      decoration: InputDecoration(
                          hintText: 'Password',
                          fillColor: Colors.white,
                          filled: true,
                          contentPadding:
                              const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(20.0),
                              borderSide: const BorderSide(
                                  color: Color.fromRGBO(255, 255, 255, 150)))),
                    ),
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                ),
                SizedBox(
                  height: size.height * (1 / 37),
                ),
                Row(children: [
                  RichText(
                      text: const TextSpan(
                          text: "   What's your ",
                          style: TextStyle(
                              color: Color.fromRGBO(55, 65, 64, 1),
                              fontWeight: FontWeight.w600,
                              fontSize: 15,
                              fontFamily: 'WorkSans'))),
                  RichText(
                      text: const TextSpan(
                          text: "birthdate",
                          style: TextStyle(
                              color: Color.fromRGBO(0, 187, 169, 1),
                              fontWeight: FontWeight.w600,
                              fontSize: 15,
                              fontFamily: 'WorkSans'))),
                  RichText(
                      text: const TextSpan(
                          text: "?",
                          style: TextStyle(
                              color: Color.fromRGBO(55, 65, 64, 1),
                              fontWeight: FontWeight.w400,
                              fontSize: 15,
                              fontFamily: 'WorkSans')))
                ]),
                SizedBox(
                  height: size.height * (1 / 37),
                ),
                SizedBox(
                  width: size.width - 50,
                  height: 30,
                  child: MaterialButton(
                    child: const Text('Birth date',
                        style: TextStyle(
                            fontSize: 14,
                            fontFamily: "WorkSans",
                            fontWeight: FontWeight.w700,
                            color: Color.fromRGBO(255, 255, 255, 1))),
                    onPressed: () => _chooseBirthdate(context),
                    color: const Color.fromRGBO(0, 187, 169, 1),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0)),
                    minWidth: size.width - 80.0,
                    height: 30,
                  ),
                ),
                SizedBox(
                  height: size.height * (1 / 16.4),
                ),
                MaterialButton(
                  child: const Text('Sign up',
                      style: TextStyle(
                          fontSize: 14,
                          fontFamily: "WorkSans",
                          fontWeight: FontWeight.w700,
                          color: Color.fromRGBO(255, 255, 255, 1))),
                  onPressed: () async {
                    double last = getTimeNow();
                    double tiempoStay = last - inicio;
                    try {
                      final tiemporegister =
                          Tiempo(name: "Sign Up", time: tiempoStay);
                      await Amplify.DataStore.save(tiemporegister);
                    } catch (e) {
                      print(e);
                    }
                    _gotoSignUpScreen(context);
                  },
                  color: const Color.fromRGBO(246, 65, 108, 1),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0)),
                  minWidth: size.width - 50.0,
                  height: 40,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _chooseBirthdate(BuildContext context) async {
    final initialDate = DateTime.now();

    final newDate = await showDatePicker(
        context: context,
        initialDate: initialDate,
        firstDate: DateTime(DateTime.now().year - 90),
        lastDate: DateTime(
          DateTime.now().year + 1,
        ));

    if (newDate != null) {
      String totaldate = newDate.toString();

      var dia = totaldate.splitMapJoin(":");

      int birthyear = int.parse(totaldate.split("-")[0]);

      int birthmonth = int.parse(totaldate.split("-")[1]);
      int birthday = int.parse(totaldate.split("-")[2].split(" ")[0]);
      String space = "-";
      _dates = birthyear.toString() +
          space +
          birthmonth.toString() +
          space +
          birthday.toString();
      String todaydate = DateTime.now().toString();

      int todayYear = int.parse(todaydate.split("-")[0]);
      int todayMonth = int.parse(todaydate.split("-")[1]);
      int todayDay = int.parse(todaydate.split("-")[2].split(" ")[0]);

      int edad = todayYear - birthyear;

      if (todayMonth - birthmonth <= 0) {
        if (todayDay - birthday <= 0) {
          edad -= 1;
        }
      }
      _age = edad;
    }
  }

  void guardar(String email, String username, String password) async {
    try {
      final users = User(
          name: _name.text,
          email: email,
          bio: _bio,
          username: username,
          birthdate: _dates,
          age: _age,
          profilePic: _profilePic,
          coverImg: _coverImg,
          interests: _interest);

      // print(_age);
      // print(email);
      // print(_name.text);

      // print("f");
      revisando = false;

      final ojala = await Amplify.DataStore.save(users);
      revisando = true;
    } catch (e) {
      // print("errorrrrrr");
      // print(e);
    }
  }

  void _gotoSignUpScreen(BuildContext context) async {
    if (result == ConnectivityResult.none) {
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
            title: const Text("There is no network connection"),
            content: const Text(
                "Please check your network connection and try again"),
            actions: <Widget>[
              FlatButton(
                child: const Text("Continue"),
                onPressed: Navigator.of(context).pop,
              )
            ]),
      );
      return;
    }
    print("1");
    final email = _emailController.text;
    final user = _userController.text;
    final password = _passwordController.text;
    // print("yaaaaay");
    // print(password);
    // print(user);
    // print(email);
    // print(_name.text);
    // print("age");
    // print(_age);
    if (_name.text != "" && email != "" && user != "" && password != "") {
      bool emailValid = RegExp(
              r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
          .hasMatch(email);
      if (emailValid == false) {
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
              title: const Text("Insert a valid email format"),
              content: const Text(
                  "A valid email format has the form email@example.com"),
              actions: <Widget>[
                FlatButton(
                  child: const Text("Continue"),
                  onPressed: Navigator.of(context).pop,
                )
              ]),
        );
      } else {
        if (_age < 5) {
          showDialog(
            context: context,
            builder: (context) => AlertDialog(
                title: const Text("Put a valid date"),
                content: const Text("You must be 5 years old or older"),
                actions: <Widget>[
                  FlatButton(
                    child: const Text("Continue"),
                    onPressed: Navigator.of(context).pop,
                  )
                ]),
          );
        } else {
          if (password.length < 8 ||
              (password.contains(RegExp(r'[a-z]')) &&
                  password.contains(RegExp('r[z0-9]')))) {
            showDialog(
              context: context,
              builder: (context) => AlertDialog(
                  title: const Text("Invalid input Password"),
                  content: const Text(
                      "Password must have 8 characters and it should have characters and numbers"),
                  actions: <Widget>[
                    FlatButton(
                      child: const Text("continue"),
                      onPressed: Navigator.of(context).pop,
                    )
                  ]),
            );
          } else {
            // print("davvvvvvv");
            print("2");
            final request = ModelQueries.list(User.classType);
            final response = await Amplify.API.query(request: request).response;
            var resultado = response.data?.items;
            print(resultado);
            bool verificar = false;
            for (int i = 0; i < resultado!.length; i++) {
              if (resultado[i]?.username == user ||
                  resultado[i]?.email == email) {
                verificar = true;
                // print("se vio");
                // print(resultado[i]?.email);
                // print(resultado[i]?.username);
              }
            }

            if (!verificar) {
              if (user.contains(" ")) {
                showDialog(
                  context: context,
                  builder: (context) => AlertDialog(
                      title: const Text("User error"),
                      content: const Text("cannot put spaces on username"),
                      actions: <Widget>[
                        FlatButton(
                          child: const Text("continue"),
                          onPressed: Navigator.of(context).pop,
                        )
                      ]),
                );
              } else {
                print("3");
                Dialogs.showLoadingDialog(context, _keyLoader);

                try {
                  print("what");
                  var signupResult = await Amplify.Auth.signUp(
                      username: user,
                      password: password,
                      options: CognitoSignUpOptions(userAttributes: {
                        CognitoUserAttributeKey.email: email
                      }));
                  guardar(email, user, password);
                  if (signupResult.isSignUpComplete) {
                    // print(email);
                    final prefs = await SharedPreferences.getInstance();
                    prefs.setString('user', user);
                    // print(user);
                    Navigator.of(_keyLoader.currentContext!,
                            rootNavigator: true)
                        .pop();
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (_) => emailpage(
                            emails: email, users: user, password: password),
                      ),
                    );
                    registerFrequency("Email Confirmation");
                  }
                } catch (e) {
                  showDialog(
                    context: context,
                    builder: (context) => AlertDialog(
                        title: const Text("Error with the username and email"),
                        content: const Text(
                            "Please check if username has the right format"),
                        actions: <Widget>[
                          FlatButton(
                            child: const Text("Continue"),
                            onPressed: Navigator.of(context).pop,
                          )
                        ]),
                  );
                }
              }
            } else {
              showDialog(
                context: context,
                builder: (context) => AlertDialog(
                    title: const Text("Username or email already exists"),
                    content: const Text(
                        "Please change the email or username that has been input"),
                    actions: <Widget>[
                      FlatButton(
                        child: const Text("Continue"),
                        onPressed: Navigator.of(context).pop,
                      )
                    ]),
              );
            }
          }
        }
      }
    } else {
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
            title: const Text("Please fill all the questions"),
            content: const Text("All the fields are required"),
            actions: <Widget>[
              FlatButton(
                child: const Text("Continue"),
                onPressed: Navigator.of(context).pop,
              )
            ]),
      );
    }
  }
}
