// import 'package:geopoint/geopoint.dart';
// ignore_for_file: file_names

import 'package:geopoint/geopoint.dart';
import 'package:latlong2/latlong.dart';

class StoreDataModel {
  //data Type
  int? id;
  String? name;
  String? images;
  String? description;
  String? website;
  String? phone;
  String? address;
  GeoPoint? storeLoc;

  List<String>? gifts;

  StoreDataModel(
      {this.id,
      this.name,
      this.images,
      this.description,
      this.website,
      this.phone,
      this.address,
      this.storeLoc,
      this.gifts});
  //method that assign values to respective datatype variables
  StoreDataModel.fromJson(Map<dynamic, dynamic> json) {
    id = json['id'];
    name = json['name'];
    images = json['images'];
    description = json['description'];
    website = json['website'];
    phone = json['phone'];
    address = json['address'];
    storeLoc = GeoPoint.fromLatLng(
        point: LatLng(json["location"]["_latitude"].toDouble(),
            json["location"]["_longitude"].toDouble()));
    gifts = List<String>.from(json['gifts']);
  }
}
