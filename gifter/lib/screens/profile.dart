// ignore_for_file: camel_case_types, non_constant_identifier_names, must_be_immutable

import 'dart:convert';
import 'dart:io';
import 'dart:isolate';
import 'package:cache_manager/cache_manager.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:amplify_api/model_queries.dart';
import 'package:amplify_auth_cognito/amplify_auth_cognito.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:gifter/models/Gift.dart';
import 'package:gifter/screens/StoreDataModel.dart';

import 'package:gifter/screens/firstScreen.dart';
import 'package:gifter/screens/friendProfile.dart';
import 'package:gifter/screens/giftFeed.dart';
import 'package:image_picker/image_picker.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../models/Tiempo.dart';
import '../models/User.dart';
import './profileEdit.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'dart:async';
import 'package:waterfall_flow/waterfall_flow.dart';
import 'averageSessionTime.dart';
import 'fallbackView.dart';
import 'frequency.dart';
import 'giftDataModel.dart';
import 'giftDetail.dart';

class profile extends StatefulWidget {
  BuildContext context;
  List<String>? giftIds;
  int donde;

  profile(this.context, {this.giftIds, Key? key, required this.donde})
      : super(key: key);

  @override
  State<profile> createState() => _profileState();
}

String usernames = "";

class _profileState extends State<profile> {
  double getTimeNow() {
    String now = DateTime.now().toString();
    String time = now.split(" ")[1];
    var arraytime = time.split(":");
    int minute = int.parse(arraytime[1]);
    int hour = int.parse(arraytime[0]);
    double seconds = double.parse(arraytime[2]);
    double timeSeconds = minute * 60 + seconds + hour * 60 * 60;
    return timeSeconds;
  }

  Future<void> calculateTime(int ver) async {
    double last = getTimeNow();
    double tiempoStay = last - inicio;
    try {
      final tiemporegister = Tiempo(name: "Profile", time: tiempoStay);
      await Amplify.DataStore.save(tiemporegister);
    } catch (e) {
      print(e);
    }
  }

  List<GiftDataModel> fetch = [];

  double inicio = 0;
  var theGifts;
  var futuro;

  @override
  void initState() {
    super.initState();
    theGifts = fetchGifts();
    inicio = getTimeNow();
    Connectivity().checkConnectivity().then((value) {
      result = value;
    });

    subscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      setState(() => this.result = result);
      if (result == ConnectivityResult.none) {
        setState(() => _showSnackBar());
      } else {
        print("aaaa");
        setState(() => ScaffoldMessenger.of(context).hideCurrentSnackBar());
        setState(() => print("aaaa"));
        UserInfo().then((result) {
          // print("result: $result");
          setState(() {});
        });
      }
      // Got a new connectivity status!
    });
    UserInfo().then((result) {
      // print("result: $result");
      setState(() {});
    });

    futuro = obtenerInfo();
  }

  var storesList = [
    StoreDataModel(
        name: "Name",
        images:
            "https://grownandflown.com/wp-content/uploads/2017/12/gifts-.jpeg",
        description: "Description",
        website: "",
        phone: "000",
        address: "",
        // location: GeoPoint(latitude: 1.2, longitude: 2.2),
        gifts: []),
    StoreDataModel(
        name: "Name",
        images:
            "https://grownandflown.com/wp-content/uploads/2017/12/gifts-.jpeg",
        description: "Description",
        website: "",
        phone: "000",
        address: "",
        // location: GeoPoint(latitude: 1.2, longitude: 2.2),
        gifts: []),
    StoreDataModel(
        name: "Name",
        images:
            "https://grownandflown.com/wp-content/uploads/2017/12/gifts-.jpeg",
        description: "Description",
        website: "",
        phone: "000",
        address: "",
        // location: GeoPoint(latitude: 1.2, longitude: 2.2),
        gifts: [])
  ];

  late int _selectedIndex;
  late List<Widget> _pages;
  List<String> Friendy = [];

  ConnectivityResult result = ConnectivityResult.none;
  late StreamSubscription subscription;
  final double coverHeight = 160;
  final double profileHeight = 120;
  String nombre = "user";
  String bio = "";
  String PictUrl = "";
  String pictIni =
      'https://happytravel.viajes/wp-content/uploads/2020/04/146-1468479_my-profile-icon-blank-profile-picture-circle-hd-300x238.png';
  String BackPict = "";
  String Backini =
      "https://img.freepik.com/vector-gratis/fondo-memphis-semitono-azul-lineas-amarillas-formas-circulos_1017-31954.jpg?w=826&t=st=1648179095~exp=1648179695~hmac=ae3c6d7af540cbf9795c6a6f3607392a6c3fc63f4206770485a400d22beffb7a";

  void _showSnackBar() {
    ScaffoldMessenger.of(context).hideCurrentSnackBar();

    SnackBar snackBar = const SnackBar(
        content: Text("There is no internet connection"),
        duration: Duration(days: 1));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  Future _image(int i, int option, BuildContext context) async {
    try {
      var tipo = ImageSource.camera;
      if (option == 1) {
        tipo = ImageSource.gallery;
      }
      final image = await ImagePicker().pickImage(source: tipo);
      if (image == null) {
        return;
      }
      final imageTemporary = File(image.path);
      File images = imageTemporary;
      final fileName = DateTime.now().toIso8601String();
      final result = await Amplify.Storage.uploadFile(
          local: images, key: fileName + '.jpg');
      final imageKey = result.key;
      final user = await Amplify.Auth.getCurrentUser();
      final imageURL = await Amplify.Storage.getUrl(key: imageKey);
      final UserObject = await Amplify.DataStore.query(User.classType,
          where: User.USERNAME.eq(user.username));

      if (i == 0) {
        setState(() {
          PictUrl = imageURL.url;
        });
        final UpdateObject = UserObject[0].copyWith(profilePic: imageKey);
        await Amplify.DataStore.save(UpdateObject);
      } else {
        setState(() {
          BackPict = imageURL.url;
        });
        print("hola");

        final UpdateObject = UserObject[0].copyWith(coverImg: imageKey);
        await Amplify.DataStore.save(UpdateObject);
      }
    } catch (e) {
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
            title: const Text("There was a problem"),
            content: const Text(
                "Please check your network connection and try again :)"),
            actions: <Widget>[
              FlatButton(
                child: const Text("Continue"),
                onPressed: () async {
                  double last = getTimeNow();
                  double tiempoStay = last - inicio;
                  try {
                    final tiemporegister =
                        Tiempo(name: "Profile", time: tiempoStay);
                    await Amplify.DataStore.save(tiemporegister);
                  } catch (e) {
                    print(e);
                  }
                  Navigator.of(context).pop;
                },
              )
            ]),
      );
    }
    if (result == ConnectivityResult.none) {
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
            title: const Text("No internet"),
            content: const Text(
                "Please check your network connection and try again :)"),
            actions: <Widget>[
              FlatButton(
                child: const Text("Continue"),
                onPressed: Navigator.of(context).pop,
              )
            ]),
      );
    }
  }

  Future<List> obtenerInfo() async {
    final prefs = await SharedPreferences.getInstance();
    var user = prefs.getString('user') ?? "error user";

    final UserObject = await Amplify.DataStore.query(User.classType,
        where: User.USERNAME.eq(user));
    var nombres = UserObject[0].friends;
    List users = [];
    for (int i = 0; i < nombres!.length; i++) {
      users.add(await Amplify.DataStore.query(User.classType,
          where: User.USERNAME.contains(nombres[i])));
    }

    return users;
  }

  Future<bool> UserInfo() async {
    var user;
    String vari = "";
    String variable = "";
    print(result.toString());
    if (result != ConnectivityResult.none) {
      user = await Amplify.Auth.getCurrentUser();
      print(user);
    } else {
      final prefs = await SharedPreferences.getInstance();
      user = prefs.getString('user') ?? "error user";
      print(user);
    }
    if (widget.donde == 1) {
      print("holaap");
      final prefs2 = await SharedPreferences.getInstance();
      var pict;
      var back;
      //pict = prefs2.getString('pict') ?? "error user";
      //back = prefs2.getString('back') ?? "error user";

      pict = await ReadCache.getString(key: "PictUrl");
      back = await ReadCache.getString(key: "BackUrl");
      print(pict);
      if (pict != "error user" && back != "error user") {
        PictUrl = pict;
        BackPict = back;
      }
      usernames = user;
    } else {
      PictUrl = pictIni;
      BackPict = Backini;
    }
    // user = "davidguarin";
    print("--");
    usernames = user;
    if ("error user" != user) {
      final UserObject = await Amplify.DataStore.query(User.classType,
          where: User.USERNAME.eq(user));
      print(UserObject);
      print(result.toString());
      if (result != ConnectivityResult.none) {
        if (UserObject.length == 0) {
          print("object");
          final request = ModelQueries.list(User.classType);
          final response = await Amplify.API.query(request: request).response;
          var resultado = response.data?.items;
          print(resultado);
          // ignore: unused_local_variable
          bool verificar = false;
          for (int i = 0; i < resultado!.length; i++) {
            // ignore: unrelated_type_equality_checks
            if (resultado[i]?.username == user) {
              nombre = resultado[i]!.name;
              bio = resultado[i]!.bio!;
              vari = resultado[i]!.profilePic!;
              variable = resultado[i]!.coverImg!;
            }
          }
        } else {
          nombre = UserObject[0].name.toString();

          bio = UserObject[0].bio.toString();

          vari = UserObject[0].profilePic.toString();
          variable = UserObject[0].coverImg.toString();
        }
      } else {
        print("entro22");
        nombre = UserObject[0].name.toString();

        bio = UserObject[0].bio.toString();

        vari = UserObject[0].profilePic.toString();
        variable = UserObject[0].coverImg.toString();
      }
    }
    if (result != ConnectivityResult.none) {
      final prefs = await SharedPreferences.getInstance();

      if (vari != "") {
        final urlpic = await Amplify.Storage.getUrl(key: vari);
        setState(() {
          PictUrl = urlpic.url;
        });
        await WriteCache.setString(key: "PictUrl", value: urlpic.url);
        //prefs.setString('pict', urlpic.url);
        // print("entro");
      }
      if (variable != "") {
        final urlpic = await Amplify.Storage.getUrl(key: variable);
        setState(() {
          BackPict = urlpic.url;
        });
        await WriteCache.setString(key: "BackUrl", value: urlpic.url);
        //prefs.setString('back', urlpic.url);
        // print("entro por dos");
      }
    }
    await imagenguardar();
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        // padding: EdgeInsets.zero,
        children: <Widget>[
          buildTop(),
          buildContent(),
          espacio(),
          FriendWidget,
          GetDivider,
          buildWishList(),
          waterfallWidget(),
        ],
      ),
      //bottomNavigationBar: buildNavigation(),
    );
  }

  Widget espacio() {
    return Padding(
        padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 16),
        child: Align(
            alignment: Alignment.centerLeft,
            child: RichText(
                text: const TextSpan(
                    text: "Friends",
                    style: TextStyle(
                        color: Color.fromRGBO(55, 65, 64, 1),
                        fontWeight: FontWeight.w600,
                        fontSize: 18,
                        fontFamily: 'WorkSans')))));
  }

  Widget buildWishList() {
    return Column(
      children: [
        Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
            child: Align(
                alignment: Alignment.centerLeft,
                child: RichText(
                    text: const TextSpan(
                        text: "Your favorite gift ideas",
                        style: TextStyle(
                            color: Color.fromRGBO(55, 65, 64, 1),
                            fontWeight: FontWeight.w600,
                            fontSize: 18,
                            fontFamily: 'WorkSans'))))),
        const SizedBox(height: 4),
        buildWishListImage(),
      ],
    );
  }

  Widget buildCircle(
          {required Widget child, required double all, required Color color}) =>
      ClipOval(
          child: Container(
        padding: EdgeInsets.all(all),
        color: color,
        child: child,
      ));
  Widget createbuildIcon() => buildCircle(
      color: Colors.grey,
      all: 2,
      child: IconButton(
        icon: const Icon(Icons.camera_alt_rounded),
        onPressed: () {
          if (result == ConnectivityResult.none) {
            showDialog(
              context: context,
              builder: (context) => AlertDialog(
                  title: const Text("No internet"),
                  content: const Text(
                      "Please check your network connection and try again :)"),
                  actions: <Widget>[
                    FlatButton(
                      child: const Text("Continue"),
                      onPressed: Navigator.of(context).pop,
                    )
                  ]),
            );
          } else {
            showSimpleDialog(context, 0);
          }
        },
        iconSize: 20,
      ));

  Widget createbuildIcon2() => buildCircle(
      color: Colors.grey,
      all: 2,
      child: IconButton(
        icon: const Icon(Icons.camera_alt_rounded),
        onPressed: () {
          if (result == ConnectivityResult.none) {
            showDialog(
              context: context,
              builder: (context) => AlertDialog(
                  title: const Text("No internet"),
                  content: const Text(
                      "Please check your network connection and try again :)"),
                  actions: <Widget>[
                    FlatButton(
                      child: const Text("Continue"),
                      onPressed: Navigator.of(context).pop,
                    )
                  ]),
            );
          } else {
            showSimpleDialog(context, 1);
          }
        },
        iconSize: 20,
      ));
  Widget buildTop() {
    final bottom = profileHeight / 2;
    return Stack(
        clipBehavior: Clip.none,
        alignment: Alignment.center,
        children: [
          Container(
            margin: EdgeInsets.only(bottom: bottom),
            child: buildCoverImage(),
          ),
          Positioned(
              top: coverHeight - 0.5 * profileHeight,
              child: Stack(children: [
                buildProfileImage(),
                Positioned(
                    bottom: 0,
                    right: 0,
                    height: 40,
                    width: 40,
                    child: createbuildIcon())
              ])),
          Positioned(
              top: coverHeight - 0.2 * profileHeight,
              right: 20,
              height: 40,
              width: 40,
              child: createbuildIcon2()),
        ]);
  }

  void showSimpleDialog(BuildContext context, int i) => showDialog(
      context: context,
      builder: (context) {
        return SimpleDialog(
          children: [
            SimpleDialogOption(
              child: RichText(
                  text: const TextSpan(
                      text: "Camera",
                      style: TextStyle(
                          color: Color.fromRGBO(0, 0, 0, 1),
                          fontWeight: FontWeight.w200,
                          fontSize: 16,
                          fontFamily: 'WorkSans'))),
              onPressed: () async {
                Navigator.pop(context, 'Gallery');
                await _image(i, 0, context);
              },
            ),
            SimpleDialogOption(
              child: RichText(
                  text: const TextSpan(
                      text: "Gallery",
                      style: TextStyle(
                          color: Color.fromRGBO(0, 0, 0, 1),
                          fontWeight: FontWeight.w200,
                          fontSize: 16,
                          fontFamily: 'WorkSans'))),
              onPressed: () async {
                Navigator.pop(context, 'Gallery');
                await _image(i, 1, context);
              },
            ),
          ],
        );
      });

  Widget buildContent() {
    return Column(
      children: [
        const SizedBox(height: 4),
        Container(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            width: MediaQuery.of(context).size.width - 16,
            child: Align(
                alignment: Alignment.center,
                child: RichText(
                    text: TextSpan(
                        text: nombre,
                        style: const TextStyle(
                            color: Color.fromRGBO(0, 0, 0, 1),
                            fontWeight: FontWeight.w700,
                            fontSize: 24,
                            fontFamily: 'WorkSans'))))),
        const SizedBox(height: 4),
        Padding(
          padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
          child: RichText(
              text: TextSpan(
                  text: bio,
                  style: const TextStyle(
                      color: Color.fromRGBO(0, 0, 0, 1),
                      fontWeight: FontWeight.w400,
                      fontSize: 15,
                      fontFamily: 'WorkSans'))),
        ),
        const SizedBox(height: 4),
        MaterialButton(
          child: const Text('Edit Profile',
              style: TextStyle(
                  fontSize: 14,
                  fontFamily: "WorkSans",
                  fontWeight: FontWeight.w700,
                  color: Color.fromRGBO(255, 255, 255, 1))),
          onPressed: () async {
            Isolate.spawn(calculateTime, 1);
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (_) => profileEdit(),
              ),
            );
            registerFrequency("Edit Profile");
          },
          color: const Color.fromRGBO(246, 65, 108, 1),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(18.0)),
          minWidth: 70,
          height: 26,
        ),
      ],
    );
  }

  Widget buildWishListImage() => Container();
  Widget buildFriendsImage() => Container();
  List<String> listaRetorno = [];

  String revisar(String url, int ind, String f) {
    if (url == null || url == "") {
      return pictIni;
    } else {
      return imagenes[ind][1];
    }
  }

  List imagenes = [];
  Future<void> imagenguardar() async {
    final prefs = await SharedPreferences.getInstance();
    var user = prefs.getString('user') ?? "error user";

    final UserObject = await Amplify.DataStore.query(User.classType,
        where: User.USERNAME.eq(user));
    var nombres = UserObject[0].friends;
    List users = [];
    for (int i = 0; i < nombres!.length; i++) {
      List unir = [];
      var rep = await Amplify.DataStore.query(User.classType,
          where: User.USERNAME.contains(nombres[i]));
      unir.add(rep[0].username);
      var imagenu = rep[0].coverImg as String;
      final imageURL = await Amplify.Storage.getUrl(key: imagenu);

      unir.add(imageURL.url);
      imagenes.add(unir);
    }
  }

  Widget buildCoverImage() => Container(
        color: Colors.grey,
        child: CachedNetworkImage(
          imageUrl: BackPict,
          placeholder: (context, url) => Center(
            child: Transform.scale(
              scale: 0.5,
              child: const CircularProgressIndicator(),
            ),
          ),
          fit: BoxFit.fill,
          width: double.infinity,
          height: coverHeight,
          errorWidget: (context, url, error) =>
              const Center(child: Icon(Icons.error)),
        ),
      );
  Widget get GetDivider {
    return const Divider(
      color: Color.fromARGB(244, 230, 230, 230),
      thickness: 8,
    );
  }

  Widget buildProfileImage() => CircleAvatar(
        radius: profileHeight / 2,
        backgroundColor: Colors.grey.shade800,
        backgroundImage: CachedNetworkImageProvider(PictUrl),
      );
  Widget get FriendWidget {
    // var gft = getGifts();
    // storeGifts(widget.index).then((value) => gft = value);
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 12),
        child: FutureBuilder<List>(
            future: futuro,
            builder: (context, data) {
              if (data.hasError) {
                return const Text(
                  "You haven't added friends. Go to Explore and search for people you may know and add them to your friends list.",
                  textAlign: TextAlign.center,
                  style:
                      TextStyle(fontFamily: "WorkSans", color: Colors.blueGrey),
                );
              } else if (data.hasData) {
                var gft = data.data;

                if (gft!.isEmpty) {
                  return const Text(
                    "You haven't added friends. Go to Explore and search for people you may know and add them to your friends list.",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontFamily: "WorkSans", color: Colors.blueGrey),
                  );
                }

                return SizedBox(
                    height: 120.0,
                    child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: gft.length,
                        itemBuilder: (context, index) {
                          return Padding(
                              padding: const EdgeInsets.only(right: 12),
                              child: InkWell(
                                  onTap: () {
                                    registerFrequency("Gift Detail");

                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (_) => friendProfile(
                                          context,
                                          donde: 0,
                                          userFriend:
                                              gft[index][0].username.toString(),
                                          // stores: gft[index].stores
                                        ),
                                      ),
                                    );
                                  },
                                  child: SizedBox(
                                      width: 100,
                                      child: Card(
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(10.0),
                                        ),
                                        elevation: 5,
                                        child: Column(
                                          // mainAxisAlignment: MainAxisAlignment.start,
                                          // crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Expanded(
                                              child: ClipRRect(
                                                borderRadius:
                                                    const BorderRadius.only(
                                                        topLeft:
                                                            Radius.circular(10),
                                                        topRight:
                                                            Radius.circular(
                                                                10)),
                                                child: CachedNetworkImage(
                                                  width: 200,
                                                  imageUrl: revisar(
                                                      gft[index][0]
                                                          .coverImg
                                                          .toString(),
                                                      index,
                                                      gft[index][0]
                                                          .username
                                                          .toString()),
                                                  fit: BoxFit.cover,
                                                  placeholder: (context, url) =>
                                                      Center(
                                                          child:
                                                              Transform.scale(
                                                    scale: 0.5,
                                                    child:
                                                        const CircularProgressIndicator(),
                                                  )),
                                                  errorWidget:
                                                      (context, url, error) =>
                                                          const Center(
                                                              child: Icon(
                                                                  Icons.error)),
                                                ),
                                              ),
                                            ),
                                            Container(
                                              padding: const EdgeInsets.all(8),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    gft[index][0]
                                                        .name
                                                        .toString(),
                                                    style: const TextStyle(
                                                        fontSize: 14,
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        fontFamily:
                                                            "Montserrat"),
                                                  )
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                      ))));
                        }));
              } else {
                return Center(
                    child: Transform.scale(
                        scale: 0.5, child: const CircularProgressIndicator()));
              }
            }));
  }

  List<bool> isPressed = List.filled(99, true);
  List<Color> heartColor = List.filled(99, Colors.redAccent);

  Widget waterfallWidget() {
    // fetchGifts();
    // if (result == ConnectivityResult.none) {
    //   return FallbackView(true, context);
    // } else {
    // if (fetch.isEmpty) {
    //   return Flexible(
    //     child: SingleChildScrollView(
    //         child: Padding(
    //             padding: const EdgeInsets.symmetric(horizontal: 48),
    //             child: Column(
    //               mainAxisAlignment: MainAxisAlignment.center,
    //               children: [
    //                 Image(
    //                   image: const AssetImage('assets/logo.png'),
    //                   height: MediaQuery.of(context).size.width / 4,
    //                 ),
    //                 const Padding(padding: EdgeInsets.only(bottom: 24)),
    //                 const Align(
    //                     alignment: Alignment.center,
    //                     child: Text(
    //                       "You have not added gifts to your wishlist. Go to the Feed and like some gifts to see them here.",
    //                       textAlign: TextAlign.center,
    //                       style:
    //                           TextStyle(fontFamily: "WorkSans", fontSize: 15),
    //                     ))
    //               ],
    //             ))),
    //   );
    // } else {
    return FutureBuilder(
        future: fetchGifts(),
        builder: (context, data) {
          if (data.hasError) {
            return FallbackView(true, context);
          } else if (data.hasData) {
            var items = data.data as List<GiftDataModel>;
            return Flexible(
                child: WaterfallFlow.builder(
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    padding: const EdgeInsets.symmetric(horizontal: 16),
                    itemCount: fetch.length,
                    gridDelegate:
                        const SliverWaterfallFlowDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      crossAxisSpacing: 8.0,
                      mainAxisSpacing: 8.0,
                    ),
                    itemBuilder: (context, index) {
                      // print("cat: ${fetch[index].stores!.runtimeType}");
                      pressed(fetch[index].id!).then((value) {
                        setState(() {
                          isPressed[index] = value;
                          heartColor[index] = isPressed[index]
                              ? Colors.redAccent
                              : Colors.white;
                        });
                      });
                      return InkWell(
                          onTap: () async {
                            Isolate.spawn(calculateTime, 1);
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (_) => GiftDetail(
                                    fetch[index].id!,
                                    fetch[index].name!,
                                    fetch[index].image!,
                                    fetch[index].description!,
                                    fetch[index].price!,
                                    fetch[index].categories!,
                                    isPressed[index],
                                    stores: storesList),
                              ),
                            );
                            registerFrequency("Gift Detail");
                          },
                          child: Stack(children: [
                            ClipRRect(
                                borderRadius: BorderRadius.circular(10.0),
                                child: CachedNetworkImage(
                                    imageUrl: fetch[index].image!,
                                    placeholder: (context, url) => Center(
                                            child: Transform.scale(
                                          scale: 0.5,
                                          child:
                                              const CircularProgressIndicator(),
                                        )),
                                    errorWidget: (context, url, error) =>
                                        const Center(child: Icon(Icons.error)),
                                    color:
                                        const Color.fromRGBO(224, 224, 224, 1),
                                    colorBlendMode: BlendMode.modulate)),
                            Align(
                                alignment: Alignment.topRight,
                                child: IconButton(
                                  splashRadius: 10,
                                  iconSize: 24,
                                  icon: Icon(Icons.favorite,
                                      color: heartColor[index]),
                                  onPressed: () {
                                    setState(() {
                                      isPressed[index] = !isPressed[index];
                                      heartColor[index] = isPressed[index]
                                          ? Colors.redAccent
                                          : Colors.white;
                                    });
                                    saveToWishlist(
                                        isPressed[index], fetch[index].id!);
                                  },
                                ))
                          ]));
                    }));
          } else {
            return const Flexible(
                child: Center(
              child: CircularProgressIndicator(),
            ));
          }
        });
    // }
    // }
  }

  Future<List<GiftDataModel>> fetchGifts() async {
    Dio dio = Dio();
    final prefs = await SharedPreferences.getInstance();
    var user = prefs.getString('user') ?? "";
    var list = prefs.getStringList("${user}Wishlist") ?? [];
    List<GiftDataModel> gifts = [];

    String fileName = "gifts.json";
    var dir = await getTemporaryDirectory();
    File file = File(dir.path + "/" + fileName);

    if (user != "") {
      if (file.existsSync()) {
        final data = file.readAsStringSync();
        final l = json.decode(data) as List<dynamic>;
        var temp = l.map((e) => GiftDataModel.fromJson(e)).toList();
        for (int i = 0; i < list.length; i++) {
          var s = temp.where((element) => element.id == list[i]);
          gifts.add(s.first);
        }
      } else {
        var model;
        for (int i = 0; i < list.length; i++) {
          final res = await dio.post(
              "https://us-central1-gifter-team26.cloudfunctions.net/getGift",
              data: jsonEncode({"giftId": list[i]}));
          if (res.statusCode == 200) {
            model = GiftDataModel(
                id: res.data["id"],
                name: res.data["name"],
                description: res.data["description"],
                image: res.data["image"],
                price: res.data["price"].toDouble(),
                categories: List<String>.from(res.data["categories"]).toList());
            gifts.add(model);
          }
        }
      }
    }
    setState(() {
      fetch = gifts;
    });
    return gifts;
  }
}

Future<List<GiftDataModel>> fetchGift2() async {
  Dio dio = Dio();

  final prefs = await SharedPreferences.getInstance();
  usernames = prefs.getString('user')!;
  var ids;
  await dio
      .post(
          "https://us-central1-gifter-team26.cloudfunctions.net/getWishlistsByUserId",
          data: jsonEncode({"userId": usernames}))
      .then((value) => ids = value.data[0]["gifts"]);

  List<GiftDataModel> gifts = [];
  GiftDataModel model;
  for (int i = 0; i < ids.length; i++) {
    await dio
        .post("https://us-central1-gifter-team26.cloudfunctions.net/getGift",
            data: jsonEncode({"giftId": ids[i]}))
        .then((value) {
      // var valor = value.data as GiftDataModel;
      model = GiftDataModel(
          id: value.data["id"],
          name: value.data["name"],
          description: value.data["description"],
          image: value.data["image"],
          price: value.data["price"].toDouble(),
          categories: List<String>.from(value.data["categories"]).toList());
      gifts.add(model);
      print(model);
    });
  }
  return gifts;
}
