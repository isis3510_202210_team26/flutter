// ignore_for_file: non_constant_identifier_names, file_names

import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map_arcgis/flutter_map_arcgis.dart';
import 'package:geopoint/geopoint.dart';
import 'package:gifter/screens/giftDataModel.dart';
import 'package:gifter/screens/giftDetail.dart';
import 'package:gifter/screens/giftFeed.dart';
import 'package:gifter/screens/listStores.dart';
import 'package:url_launcher/url_launcher.dart';
// ignore: library_prefixes
import 'package:latlong2/latlong.dart' as latLng;

import 'fallbackView.dart';
import 'frequency.dart';

// ignore: must_be_immutable
class StoreDetail extends StatefulWidget {
  String? name;
  String? description;
  String? image;
  GeoPoint? loc;
  String? website;
  String? phone;
  String? address;
  List<GiftDataModel>? gifts;
  int index;

  StoreDetail(this.name, this.description, this.image, this.loc, this.website,
      this.phone, this.address, this.index,
      {this.gifts, Key? key})
      : super(key: key);

  @override
  State<StoreDetail> createState() => _StoreDetailState();
}

class _StoreDetailState extends State<StoreDetail> {
  late int _selectedIndex;
  late List<Widget> _pages;

  late Size size;
  // var gft;

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;

    // final Size size = MediaQuery.of(context).size;
    return WillPopScope(
        onWillPop: () {
          Navigator.pop(context, false);
          registerFrequency("Stores");
          return Future.value(false);
        },
        child: SafeArea(
            child: Scaffold(
          extendBodyBehindAppBar: true,
          // appBar: AppBar(
          //   foregroundColor: Colors.transparent,
          //   backgroundColor: Colors.transparent,
          //   leading: IconButton(
          //     icon: Icon(Icons.arrow_back, color: Color.fromRGBO(246, 65, 108, 1)),
          //     onPressed: () => Navigator.of(context).pop(),
          //   ),
          //   elevation: 0,
          // ),
          body: ListView(
            children: [
              Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
                CachedNetworkImage(
                  height: 250,
                  imageUrl: widget.image.toString(),
                  placeholder: (context, url) => Center(
                    child: Transform.scale(
                      scale: 0.5,
                      child: const CircularProgressIndicator(),
                    ),
                  ),
                  fit: BoxFit.cover,
                  errorWidget: (context, url, error) =>
                      const Center(child: Icon(Icons.error)),
                ),
                //const Padding(padding: EdgeInsets.only(top: 8)),
                StoreInfo,
                const Padding(padding: EdgeInsets.only(top: 8)),
                const Divider(
                  color: Color.fromARGB(244, 230, 230, 230),
                  thickness: 8,
                ),
                Location,
              ])
            ],
          ),
          // bottomNavigationBar: buildNavigation(),
        )));
  }

  var gifts;

  void initState() {
    super.initState();
    gifts = storeGifts(widget.index);
  }

  Widget get StoreInfo {
    return Container(
      width: size.width,
      padding: const EdgeInsets.all(16),
      child: Column(children: [
        Align(
            alignment: Alignment.centerLeft,
            child: Text(
              widget.name!,
              style: const TextStyle(
                  fontFamily: "Montserrat",
                  fontWeight: FontWeight.w600,
                  fontSize: 20),
            )),
        const Padding(padding: EdgeInsets.only(top: 12)),
        Align(
            alignment: Alignment.centerLeft,
            child: Text(
              widget.description!,
              style: const TextStyle(
                  fontFamily: "WorkSans",
                  fontWeight: FontWeight.w400,
                  fontSize: 14),
            )),
        const Padding(padding: EdgeInsets.only(top: 21)),
        StoreGifts,
        DetailedInfo
      ]),
    );
  }

  // Future<List<GiftDataModel>> getGifts() async {
  //   Future<List<GiftDataModel>> list;
  //   int l = widget.gifts!.length < 5 ? widget.gifts!.length : 5;
  //   for (int i = 0; i < l; i++) {
  //     await http.get(Uri.parse("url"));
  //   }

  // }

  Widget get StoreGifts {
    // var gft = getGifts();
    // storeGifts(widget.index).then((value) => gft = value);
    return FutureBuilder(
        future: gifts,
        builder: (context, data) {
          if (data.hasError) {
            return const Text(
              "There was an error trying to show the store gifts. Please check your connection and try again later.",
              textAlign: TextAlign.center,
              style: TextStyle(fontFamily: "WorkSans", color: Colors.blueGrey),
            );
          } else if (data.hasData) {
            var gft = data.data as List<GiftDataModel>;
            return SizedBox(
                height: 120.0,
                child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: 5,
                    itemBuilder: (context, index) {
                      var p;
                      pressed(gft[index].id!).then((value) => p = value);
                      return Container(
                          padding: const EdgeInsets.only(right: 8),
                          child: InkWell(
                              onTap: () {
                                registerFrequency("Gift Detail");
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (_) => GiftDetail(
                                      gft[index].id!,
                                      gft[index].name!,
                                      gft[index].image!,
                                      gft[index].description!,
                                      gft[index].price!,
                                      gft[index].categories!,
                                      p ?? false,
                                      // stores: gft[index].stores
                                    ),
                                  ),
                                );
                              },
                              child: ClipRRect(
                                  borderRadius: BorderRadius.circular(7),
                                  child: CachedNetworkImage(
                                    imageUrl: gft[index].image.toString(),
                                    height: 120,
                                    placeholder: (context, url) => Center(
                                      child: Transform.scale(
                                        scale: 0.5,
                                        child:
                                            const CircularProgressIndicator(),
                                      ),
                                    ),
                                    fit: BoxFit.fill,
                                    errorWidget: (context, url, error) =>
                                        const Center(child: Icon(Icons.error)),
                                  ))));
                    }));
          } else {
            return Center(
                child: Transform.scale(
                    scale: 0.5, child: CircularProgressIndicator()));
          }
        });
  }

  Widget get DetailedInfo {
    var url = widget.website!.contains("https://")
        ? widget.website
        : 'https://${widget.website}';
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 21),
          child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
            const Text("Address: ",
                style: TextStyle(
                    fontFamily: "WorkSans",
                    fontWeight: FontWeight.w600,
                    fontSize: 14)),
            Text(widget.address!,
                style: const TextStyle(
                    fontFamily: "WorkSans",
                    fontWeight: FontWeight.w400,
                    fontSize: 14))
          ]),
        ),
        Padding(
            padding: const EdgeInsets.only(top: 8),
            child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
              const Text("Phone: ",
                  style: TextStyle(
                      fontFamily: "WorkSans",
                      fontWeight: FontWeight.w600,
                      fontSize: 14)),
              Text(widget.phone!.toString(),
                  style: const TextStyle(
                      fontFamily: "WorkSans",
                      fontWeight: FontWeight.w400,
                      fontSize: 14))
            ])),
        Padding(
            padding: const EdgeInsets.only(top: 8),
            child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
              const Text("Website: ",
                  style: TextStyle(
                      fontFamily: "WorkSans",
                      fontWeight: FontWeight.w600,
                      fontSize: 14)),
              InkWell(
                  onTap: () {
                    _launchURL(url);
                  },
                  child: Text(url!,
                      style: const TextStyle(
                          fontFamily: "WorkSans",
                          fontWeight: FontWeight.w400,
                          fontSize: 14,
                          decoration: TextDecoration.underline,
                          color: Color.fromRGBO(0, 184, 169, 1))))
            ]))
      ],
    );
  }

  Widget get Location {
    //var url = 'https://maps.google.com/?q=${widget.long},${widget.lat}';
    var url =
        'https://www.google.com/maps/search/?api=1&query=${widget.loc!.longitude}%2C${widget.loc!.latitude}';
    return Container(
      padding: const EdgeInsets.all(16),
      child: Column(children: [
        Row(
          // ignore: prefer_const_literals_to_create_immutables
          children: [
            const Text(
              "Location",
              style: TextStyle(
                  fontFamily: "Montserrat-Regular",
                  fontWeight: FontWeight.w600,
                  fontSize: 16),
            )
          ],
        ),
        const Padding(padding: EdgeInsets.only(top: 6)),
        InkWell(
            onTap: () {
              _launchURL(url);
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              // ignore: prefer_const_literals_to_create_immutables
              children: [
                const Text(
                  "Open in Google Maps",
                  style: TextStyle(
                      fontFamily: "Montserrat-Regular",
                      fontWeight: FontWeight.w600,
                      fontSize: 14,
                      color: Color.fromRGBO(0, 184, 169, 1)),
                ),
                const Padding(padding: EdgeInsets.only(right: 4)),
                const Icon(
                  Icons.open_in_new,
                  color: Color.fromRGBO(0, 184, 169, 1),
                  size: 14,
                )
              ],
            )),
        const Padding(padding: EdgeInsets.only(top: 4)),
        // Location Maps
        // result == ConnectivityResult.none
        //     ? SizedBox(height: 300, child: FallbackView(true))
        //     :
        StoreMap()
      ]),
    );
  }

  ConnectivityResult result = ConnectivityResult.none;
  late StreamSubscription subscription;

  Widget StoreMap() {
    try {
      return SizedBox(
        height: 200,
        child: FlutterMap(
          options: MapOptions(
            center:
                // widget.location!.toLatLng(),
                latLng.LatLng(widget.loc!.longitude, widget.loc!.latitude),
            zoom: 16.0,
            plugins: [EsriPlugin()],
          ),
          layers: [
            TileLayerOptions(
              urlTemplate: 'http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}',
              subdomains: ['mt0', 'mt1', 'mt2', 'mt3'],
            ),
            FeatureLayerOptions(
              "https://services.arcgis.com/Wl7Y1m92PbjtJs5n/ArcGIS/rest/services/service_2cf7aa14a1d24cdc9fcb2e1c437c64b9/FeatureServer/0",
              "point",
              render: (dynamic attributes) {
                return Marker(
                  width: 100.0,
                  height: 100.0,
                  // ignore: prefer_const_constructors
                  builder: (ctx) => Icon(
                    Icons.pin_drop,
                    color: Colors.red,
                  ),
                  point:
                      // widget.location!.toLatLng()!,
                      latLng.LatLng(
                          widget.loc!.longitude, widget.loc!.latitude),
                );
              },
              onTap: (attributes, latLng.LatLng location) {},
            ),
          ],
        ),
      );
    } catch (e) {
      return FallbackView(true, context);
    }
  }

  void _launchURL(url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  Widget buildNavigation() {
    return BottomNavigationBar(
      selectedItemColor: const Color.fromRGBO(246, 65, 108, 1),
      unselectedItemColor: const Color.fromRGBO(196, 196, 196, 1),
      currentIndex: _selectedIndex,
      // ignore: prefer_const_literals_to_create_immutables
      items: [
        const BottomNavigationBarItem(
          icon: Icon(Icons.home),
          label: 'Feed',
        ),
        const BottomNavigationBarItem(
          icon: Icon(Icons.storefront),
          label: 'Stores',
        ),
        const BottomNavigationBarItem(
          icon: Icon(Icons.search),
          label: 'Search',
        ),
        const BottomNavigationBarItem(
          icon: Icon(Icons.person),
          label: 'Profile',
        ),
        const BottomNavigationBarItem(
          icon: Icon(Icons.menu),
          label: 'Menu',
        ),
      ],
      onTap: _onItemTapped,
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
    Navigator.push(
      context,
      MaterialPageRoute(builder: (_) => _pages.elementAt(_selectedIndex)),
    );
  }
}
