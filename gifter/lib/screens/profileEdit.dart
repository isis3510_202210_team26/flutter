// ignore_for_file: file_names, camel_case_types, unused_field, unused_local_variable, non_constant_identifier_names

import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import '../models/Tiempo.dart';
import '../models/User.dart';
import 'dart:async';
import './profile.dart';
import 'frequency.dart';

// ignore: must_be_immutable
class profileEdit extends StatefulWidget {
  profileEdit({Key? key}) : super(key: key);

  @override
  State<profileEdit> createState() => _profileEditState();
}

class _profileEditState extends State<profileEdit> {
  ConnectivityResult result = ConnectivityResult.none;
  late StreamSubscription subscription;
  final TextEditingController _name = TextEditingController();

  final TextEditingController _userController = TextEditingController();

  final TextEditingController _passwordController = TextEditingController();

  final TextEditingController _bio = TextEditingController();

  String _dates = "";

  int _age = 0;
  double getTimeNow() {
    String now = DateTime.now().toString();
    String time = now.split(" ")[1];
    var arraytime = time.split(":");
    int minute = int.parse(arraytime[1]);
    int hour = int.parse(arraytime[0]);
    double seconds = double.parse(arraytime[2]);
    double timeSeconds = minute * 60 + seconds + hour * 60 * 60;
    return timeSeconds;
  }

  double inicio = 0;
  final List<String> friend = [];
  @override
  void initState() {
    super.initState();
    inicio = getTimeNow();
    // registerFrequency("Edit Profile");
    Connectivity().checkConnectivity().then((value) {
      this.result = value;
    });
    subscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      setState(() => this.result = result);
      if (result == ConnectivityResult.none) {
        setState(() => _showSnackBar());
      } else {
        setState(() => ScaffoldMessenger.of(context).hideCurrentSnackBar());
      }
      // Got a new connectivity status!
    });
  }

  dispose() {
    super.dispose();

    subscription.cancel();
  }

  void _showSnackBar() {
    ScaffoldMessenger.of(context).hideCurrentSnackBar();

    SnackBar snackBar = const SnackBar(
        content: Text("There is no internet connection"),
        duration: Duration(days: 1));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.transparent,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back,
              color: Color.fromRGBO(246, 65, 108, 1)),
          onPressed: () {
            Navigator.of(context).pop();
            registerFrequency("Profile");
          },
        ),
        title: RichText(
            text: const TextSpan(
                text: "Edit Profile",
                style: TextStyle(
                    color: Color.fromRGBO(246, 65, 108, 1),
                    fontWeight: FontWeight.w600,
                    fontSize: 17,
                    fontFamily: 'WorkSans'))),
        elevation: 0.0,
      ),
      body: SingleChildScrollView(
        child: Form(
          child: SizedBox(
            width: size.width,
            height: size.height,
            child: Column(
              children: [
                SizedBox(
                  height: size.height * (1 / 29.6),
                ),
                Row(children: [
                  RichText(
                      text: const TextSpan(
                          text: "   What's your ",
                          style: TextStyle(
                              color: Color.fromRGBO(55, 65, 64, 1),
                              fontWeight: FontWeight.w600,
                              fontSize: 15,
                              fontFamily: 'WorkSans'))),
                  RichText(
                      text: const TextSpan(
                          text: "name",
                          style: TextStyle(
                              color: Color.fromRGBO(0, 187, 169, 1),
                              fontWeight: FontWeight.w600,
                              fontSize: 15,
                              fontFamily: 'WorkSans'))),
                  RichText(
                      text: const TextSpan(
                          text: "?",
                          style: TextStyle(
                              color: Color.fromRGBO(55, 65, 64, 1),
                              fontWeight: FontWeight.w400,
                              fontSize: 15,
                              fontFamily: 'WorkSans')))
                ]),
                SizedBox(
                  height: size.height * (1 / 37),
                ),
                SizedBox(
                  width: size.width - 50,
                  height: size.height * (1 / 18.5),
                  child: Material(
                    elevation: 2.0,
                    shadowColor: Colors.grey,
                    child: TextFormField(
                      autofocus: false,
                      controller: _name,
                      inputFormatters: [
                        LengthLimitingTextInputFormatter(30),
                      ],
                      decoration: InputDecoration(
                          hintText: 'Name',
                          fillColor: Colors.white,
                          filled: true,
                          contentPadding:
                              const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(20.0),
                              borderSide: const BorderSide(
                                  color: Color.fromRGBO(255, 255, 255, 150)))),
                    ),
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                ),
                SizedBox(
                  height: size.height * (1 / 37),
                ),
                SizedBox(
                  height: size.height * (1 / 37),
                ),
                SizedBox(
                  height: size.height * (1 / 37),
                ),
                Row(children: [
                  RichText(
                      text: const TextSpan(
                          text: "   What's your ",
                          style: TextStyle(
                              color: Color.fromRGBO(55, 65, 64, 1),
                              fontWeight: FontWeight.w600,
                              fontSize: 15,
                              fontFamily: 'WorkSans'))),
                  RichText(
                      text: const TextSpan(
                          text: "birthdate",
                          style: TextStyle(
                              color: Color.fromRGBO(0, 187, 169, 1),
                              fontWeight: FontWeight.w600,
                              fontSize: 15,
                              fontFamily: 'WorkSans'))),
                  RichText(
                      text: const TextSpan(
                          text: "?",
                          style: TextStyle(
                              color: Color.fromRGBO(55, 65, 64, 1),
                              fontWeight: FontWeight.w400,
                              fontSize: 15,
                              fontFamily: 'WorkSans')))
                ]),
                SizedBox(
                  height: size.height * (1 / 37),
                ),
                SizedBox(
                  width: size.width - 50,
                  height: 30,
                  child: MaterialButton(
                    child: const Text('Birth date',
                        style: TextStyle(
                            fontSize: 14,
                            fontFamily: "WorkSans",
                            fontWeight: FontWeight.w700,
                            color: Color.fromRGBO(255, 255, 255, 1))),
                    onPressed: () => _chooseBirthdate(context),
                    color: const Color.fromRGBO(0, 187, 169, 1),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0)),
                    minWidth: size.width - 80.0,
                    height: 30,
                  ),
                ),
                SizedBox(
                  height: size.height * (1 / 16.4),
                ),
                Row(children: [
                  RichText(
                      text: const TextSpan(
                          text: "   Tell others ",
                          style: TextStyle(
                              color: Color.fromRGBO(55, 65, 64, 1),
                              fontWeight: FontWeight.w600,
                              fontSize: 15,
                              fontFamily: 'WorkSans'))),
                  RichText(
                      text: const TextSpan(
                          text: "about you",
                          style: TextStyle(
                              color: Color.fromRGBO(0, 187, 169, 1),
                              fontWeight: FontWeight.w600,
                              fontSize: 15,
                              fontFamily: 'WorkSans'))),
                  RichText(
                      text: const TextSpan(
                          text: "!",
                          style: TextStyle(
                              color: Color.fromRGBO(55, 65, 64, 1),
                              fontWeight: FontWeight.w400,
                              fontSize: 15,
                              fontFamily: 'WorkSans')))
                ]),
                SizedBox(
                  height: size.height * (1 / 37),
                ),
                SizedBox(
                  width: size.width - 50,
                  height: size.height * (1 / 18.5),
                  child: Material(
                    elevation: 2.0,
                    shadowColor: Colors.grey,
                    child: TextFormField(
                      autofocus: false,
                      controller: _bio,
                      inputFormatters: [
                        LengthLimitingTextInputFormatter(300),
                      ],
                      decoration: InputDecoration(
                          hintText: 'Write your Bio',
                          fillColor: Colors.white,
                          filled: true,
                          contentPadding:
                              const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(20.0),
                              borderSide: const BorderSide(
                                  color: Color.fromRGBO(255, 255, 255, 150)))),
                    ),
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                ),
                SizedBox(
                  height: size.height * (1 / 16.4),
                ),
                MaterialButton(
                  child: const Text('Update Profile',
                      style: TextStyle(
                          fontSize: 14,
                          fontFamily: "WorkSans",
                          fontWeight: FontWeight.w700,
                          color: Color.fromRGBO(255, 255, 255, 1))),
                  onPressed: () async {
                    double last = getTimeNow();
                    double tiempoStay = last - inicio;
                    try {
                      final tiemporegister =
                          Tiempo(name: "Edit Profile", time: tiempoStay);
                      await Amplify.DataStore.save(tiemporegister);
                    } catch (e) {
                      print(e);
                    }
                    UpdateProfile(context);
                  },
                  color: const Color.fromRGBO(246, 65, 108, 1),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0)),
                  minWidth: size.width - 50.0,
                  height: 40,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  bool click = false;

  void _chooseBirthdate(BuildContext context) async {
    final initialDate = DateTime.now();
    click = true;
    final newDate = await showDatePicker(
        context: context,
        initialDate: initialDate,
        firstDate: DateTime(DateTime.now().year - 90),
        lastDate: DateTime(
          DateTime.now().year + 1,
        ));

    if (newDate != null) {
      String totaldate = newDate.toString();

      var dia = totaldate.splitMapJoin(":");

      int birthyear = int.parse(totaldate.split("-")[0]);

      int birthmonth = int.parse(totaldate.split("-")[1]);
      int birthday = int.parse(totaldate.split("-")[2].split(" ")[0]);
      String space = "-";
      _dates = birthyear.toString() +
          space +
          birthmonth.toString() +
          space +
          birthday.toString();
      String todaydate = DateTime.now().toString();

      int todayYear = int.parse(todaydate.split("-")[0]);
      int todayMonth = int.parse(todaydate.split("-")[1]);
      int todayDay = int.parse(todaydate.split("-")[2].split(" ")[0]);

      int edad = todayYear - birthyear;

      if (todayMonth - birthmonth <= 0) {
        if (todayDay - birthday <= 0) {
          edad -= 1;
        }
      }
      _age = edad;
    }
  }

  void UpdateProfile(BuildContext context) async {
    try {
      // print("--------------");
      var resultado = await Amplify.Auth.getCurrentUser();
      // print("--------------");
      // print(resultado.username);
      // print("--------------");
      // print(_name.text);
      final UserObject = await Amplify.DataStore.query(User.classType,
          where: User.USERNAME.eq(resultado.username));
      // print("-----------------------------------------");

      // print(UserObject);
      // print("a");
      bool verificar = true;
      User UpdateObject = UserObject[0];
      if (_dates != null) {
        if (click == true) {
          if (_age < 5) {
            verificar = false;
            showDialog(
              context: context,
              builder: (context) => AlertDialog(
                  title: const Text("Put a valid date"),
                  content: const Text("You must be 5 years old or older"),
                  actions: <Widget>[
                    FlatButton(
                      child: const Text("Continue"),
                      onPressed: Navigator.of(context).pop,
                    )
                  ]),
            );
            if (verificar == true) {
              UpdateObject = UpdateObject.copyWith(birthdate: _dates);
              UpdateObject = UpdateObject.copyWith(age: _age);
            }
          }
        }
      }
      if (_name.text != "") {
        if (verificar == true) {
          UpdateObject = UpdateObject.copyWith(name: _name.text);
        }
      }

      // ignore: unnecessary_null_comparison

      if (_bio.text != "") {
        // print("eeeeeee");
        // print(_bio);
        if (verificar == true) {
          UpdateObject = UpdateObject.copyWith(bio: _bio.text);
        }
      }
      if (verificar == true) {
        await Amplify.DataStore.save(UpdateObject);
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (_) => profile(context, donde: 1),
          ),
        );
        registerFrequency("Profile");
      }
    } catch (e) {
      // print("dddddddddddddddddddddddddddddddddddd");
      // print(e);
    }
  }
}
