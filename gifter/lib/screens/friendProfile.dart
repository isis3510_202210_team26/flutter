// ignore_for_file: camel_case_types, non_constant_identifier_names

import 'dart:convert';
import 'dart:io';
import 'dart:isolate';
import 'package:amplify_api/model_queries.dart';
import 'package:amplify_auth_cognito/amplify_auth_cognito.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:flutter/services.dart' as rootBundle;
import 'package:gifter/models/Gift.dart';
import 'package:gifter/screens/StoreDataModel.dart';
import 'package:cache_manager/cache_manager.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:gifter/screens/firstScreen.dart';
import 'package:gifter/screens/giftFeed.dart';
import 'package:gifter/screens/goodFeed.dart';
import 'package:gifter/screens/listStores.dart';
import 'package:image_picker/image_picker.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../models/Tiempo.dart';
import '../models/User.dart';
import './profileEdit.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'dart:async';
import 'package:waterfall_flow/waterfall_flow.dart';
import 'package:http/http.dart' as http;
import 'frequency.dart';
import 'giftDataModel.dart';
import 'giftDetail.dart';

class friendProfile extends StatefulWidget {
  BuildContext context;
  List<String>? giftIds;
  String userFriend;
  final int donde;

  friendProfile(this.context,
      {this.giftIds, Key? key, required this.userFriend, required this.donde})
      : super(key: key);

  @override
  State<friendProfile> createState() => _friendProfileState();
}

String usernames = "";
String friendy = "";

class _friendProfileState extends State<friendProfile> {
  double getTimeNow() {
    String now = DateTime.now().toString();
    String time = now.split(" ")[1];
    var arraytime = time.split(":");
    int minute = int.parse(arraytime[1]);
    int hour = int.parse(arraytime[0]);
    double seconds = double.parse(arraytime[2]);
    double timeSeconds = minute * 60 + seconds + hour * 60 * 60;
    return timeSeconds;
  }

  Color addFriendColor = Color.fromRGBO(246, 65, 108, 1);
  String addFriendText = "Add Friend";

  Future<void> _saveFriend() async {
    var usernameFriend = widget.userFriend;
    print(usernameFriend);
    final pref = await SharedPreferences.getInstance();
    var usernamesave = pref.getString("user");
    print(usernamesave);
    final UserObject = await Amplify.DataStore.query(User.classType,
        where: User.USERNAME.eq(usernamesave));
    List<String> listaFriend;
    var check = UserObject[0].friends;
    if (check == null) {
      listaFriend = [];
    } else {
      listaFriend = UserObject[0].friends!.toList();
    }

    if (!listaFriend.contains(usernameFriend)) {
      print("a");
      listaFriend.add(usernameFriend);
      var UpdateObject = UserObject[0].copyWith(friends: listaFriend);
      await Amplify.DataStore.save(UpdateObject);
      setState(() {
        addFriendColor = Colors.grey;
        addFriendText = "Unfriend";
      });
    } else {
      listaFriend.remove(usernameFriend);
      var UpdateObject = UserObject[0].copyWith(friends: listaFriend);
      await Amplify.DataStore.save(UpdateObject);
      setState(() {
        addFriendColor = Color.fromRGBO(246, 65, 108, 1);
        addFriendText = "Add Friend";
      });
    }
  }

  Future<void> calculateTime(int ver) async {
    double last = getTimeNow();
    double tiempoStay = last - inicio;
    try {
      final tiemporegister = Tiempo(name: "Profile", time: tiempoStay);
      await Amplify.DataStore.save(tiemporegister);
    } catch (e) {
      print(e);
    }
  }

  var fetch;

  double inicio = 0;
  @override
  void initState() {
    super.initState();
    print("giftIds: $giftIds");
    friendy = widget.userFriend;
    fetch = fetchGift2();
    inicio = getTimeNow();
    // registerFrequency("Profile");
    Connectivity().checkConnectivity().then((value) {
      this.result = value;
    });

    subscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      setState(() => this.result = result);
      if (result == ConnectivityResult.none) {
        setState(() => _showSnackBar());
      } else {
        setState(() => ScaffoldMessenger.of(context).hideCurrentSnackBar());
        UserInfo().then((result) {
          // print("result: $result");
          setState(() {});
        });
      }
      // Got a new connectivity status!
    });
    UserInfo().then((result) {
      // print("result: $result");
      setState(() {});
    });
  }

  var storesList = [
    StoreDataModel(
        name: "Name",
        images:
            "https://grownandflown.com/wp-content/uploads/2017/12/gifts-.jpeg",
        description: "Description",
        website: "",
        phone: "000",
        address: "",
        // location: GeoPoint(latitude: 1.2, longitude: 2.2),
        gifts: []),
    StoreDataModel(
        name: "Name",
        images:
            "https://grownandflown.com/wp-content/uploads/2017/12/gifts-.jpeg",
        description: "Description",
        website: "",
        phone: "000",
        address: "",
        // location: GeoPoint(latitude: 1.2, longitude: 2.2),
        gifts: []),
    StoreDataModel(
        name: "Name",
        images:
            "https://grownandflown.com/wp-content/uploads/2017/12/gifts-.jpeg",
        description: "Description",
        website: "",
        phone: "000",
        address: "",
        // location: GeoPoint(latitude: 1.2, longitude: 2.2),
        gifts: [])
  ];

  late int _selectedIndex;
  late List<Widget> _pages;
  ConnectivityResult result = ConnectivityResult.none;
  late StreamSubscription subscription;
  final double coverHeight = 160;
  final double profileHeight = 120;
  String nombre = "user";
  String bio = "";
  String PictUrl = "";
  String pictIni =
      'https://happytravel.viajes/wp-content/uploads/2020/04/146-1468479_my-profile-icon-blank-profile-picture-circle-hd-300x238.png';
  String BackPict = "";
  String Backini =
      "https://img.freepik.com/vector-gratis/fondo-memphis-semitono-azul-lineas-amarillas-formas-circulos_1017-31954.jpg?w=826&t=st=1648179095~exp=1648179695~hmac=ae3c6d7af540cbf9795c6a6f3607392a6c3fc63f4206770485a400d22beffb7a";

  void _showSnackBar() {
    ScaffoldMessenger.of(context).hideCurrentSnackBar();

    SnackBar snackBar = const SnackBar(
        content: Text("There is no internet connection"),
        duration: Duration(days: 1));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  Future<bool> UserInfo() async {
    var user;
    List listaFriend;
    String vari = "";
    String variable = "";
    print(result.toString());
    final prefs3 = await SharedPreferences.getInstance();
    var user2 = prefs3.getString("user");
    user = widget.userFriend;
    if (widget.donde == 1) {
      final prefs2 = await SharedPreferences.getInstance();

      var pict;
      var back;
      // pict = prefs2.getString('pict') ?? "error user";
      //back = prefs2.getString('back') ?? "error user";
      pict = ReadCache.getString(key: "PictUrl");
      back = ReadCache.getString(key: "BackUrl");

      if (pict != null && back != null) {
        PictUrl = pict;
        BackPict = back;
      }
    } else {
      PictUrl = pictIni;
      BackPict = Backini;
    }
    print(PictUrl);
    print("rev");
    // user = "davidguarin";
    usernames = user;
    friendy = widget.userFriend;
    if ("error user" != user) {
      final UserObject = await Amplify.DataStore.query(User.classType,
          where: User.USERNAME.eq(user));
      final UserObject2 = await Amplify.DataStore.query(User.classType,
          where: User.USERNAME.eq(user2));
      if (result != ConnectivityResult.none) {
        if (UserObject.length == 0) {
          print("object");
          final request = ModelQueries.list(User.classType);
          final response = await Amplify.API.query(request: request).response;
          var resultado = response.data?.items;
          print(resultado);
          // ignore: unused_local_variable
          bool verificar = false;
          for (int i = 0; i < resultado!.length; i++) {
            // ignore: unrelated_type_equality_checks
            if (resultado[i]?.username == user) {
              nombre = resultado[i]!.name;
              bio = resultado[i]!.bio!;
              vari = resultado[i]!.profilePic!;
              variable = resultado[i]!.coverImg!;
            }
          }

          var check = UserObject2[0].friends;
          if (check == null) {
            listaFriend = [];
          } else {
            listaFriend = UserObject2[0].friends!.toList();
          }
          if (listaFriend.contains(widget.userFriend)) {
            addFriendColor = Colors.grey;
            addFriendText = "Unfriend";
          }
          print("unfriend");
        } else {
          nombre = UserObject[0].name.toString();

          bio = UserObject[0].bio.toString();

          vari = UserObject[0].profilePic.toString();
          variable = UserObject[0].coverImg.toString();

          var check = UserObject2[0].friends;
          print(check);
          print(UserObject2[0]);
          if (check == null) {
            listaFriend = [];
          } else {
            listaFriend = UserObject2[0].friends!.toList();
          }

          if (listaFriend.contains(widget.userFriend)) {
            addFriendColor = Colors.grey;
            addFriendText = "Unfriend";
            print("aunf");
          }
          print("unfriend2");
        }
      } else {
        nombre = UserObject[0].name.toString();

        bio = UserObject[0].bio.toString();

        vari = UserObject[0].profilePic.toString();
        variable = UserObject[0].coverImg.toString();
      }
    }
    if (result != ConnectivityResult.none) {
      //final prefs = await SharedPreferences.getInstance();

      if (vari != "") {
        final urlpic = await Amplify.Storage.getUrl(key: vari);
        setState(() {
          PictUrl = urlpic.url;
        });
        await WriteCache.setString(key: "PictUrl", value: urlpic.url);
        // prefs.setString('pict', urlpic.url);
        // print("entro");
      }
      if (variable != "") {
        final urlpic = await Amplify.Storage.getUrl(key: variable);
        setState(() {
          BackPict = urlpic.url;
        });
        await WriteCache.setString(key: "BackUrl", value: urlpic.url);
        //prefs.setString('back', urlpic.url);
        // print("entro por dos");
      }
    }

    return true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        // padding: EdgeInsets.zero,
        children: <Widget>[
          buildTop(),
          buildContent(),
          buildWishList(),
          waterfallWidget(),
        ],
      ),
      //bottomNavigationBar: buildNavigation(),
    );
  }

  Widget buildNavigation() {
    return BottomNavigationBar(
      selectedItemColor: const Color.fromRGBO(246, 65, 108, 1),
      unselectedItemColor: const Color.fromRGBO(196, 196, 196, 1),
      currentIndex: _selectedIndex,
      // ignore: prefer_const_literals_to_create_immutables
      items: [
        const BottomNavigationBarItem(
          icon: Icon(Icons.home),
          label: 'Feed',
        ),
        const BottomNavigationBarItem(
          icon: Icon(Icons.storefront),
          label: 'Stores',
        ),
        const BottomNavigationBarItem(
          icon: Icon(Icons.search),
          label: 'Search',
        ),
        const BottomNavigationBarItem(
          icon: Icon(Icons.person),
          label: 'Profile',
        ),
        const BottomNavigationBarItem(
          icon: Icon(Icons.menu),
          label: 'Menu',
        ),
      ],
      onTap: _onItemTapped,
    );
  }

  void _onItemTapped(int index) {
    Isolate.spawn(calculateTime, 1);
    setState(() {
      _selectedIndex = index;
    });
    Navigator.push(
      context,
      MaterialPageRoute(builder: (_) => _pages.elementAt(_selectedIndex)),
    );
  }

  Widget buildWishList() {
    return Column(
      children: [
        Padding(
            padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
            child: Align(
                alignment: Alignment.centerLeft,
                child: RichText(
                    text: TextSpan(
                        text: nombre + "'s favorite gift ideas",
                        style: const TextStyle(
                            color: Color.fromRGBO(55, 65, 64, 1),
                            fontWeight: FontWeight.w600,
                            fontSize: 18,
                            fontFamily: 'WorkSans'))))),
        const SizedBox(height: 4),
        buildWishListImage(),
      ],
    );
  }

  Widget buildCircle(
          {required Widget child, required double all, required Color color}) =>
      ClipOval(
          child: Container(
        padding: EdgeInsets.all(all),
        color: color,
        child: child,
      ));

  Widget buildTop() {
    final bottom = profileHeight / 2;
    return Stack(
        clipBehavior: Clip.none,
        alignment: Alignment.center,
        children: [
          Container(
            margin: EdgeInsets.only(bottom: bottom),
            child: buildCoverImage(),
          ),
          Positioned(
              top: coverHeight - 0.5 * profileHeight,
              child: Stack(children: [
                buildProfileImage(),
              ])),
        ]);
  }

  Widget buildContent() {
    return Column(
      children: [
        const SizedBox(height: 4),
        RichText(
            text: TextSpan(
                text: nombre,
                style: const TextStyle(
                    color: Color.fromRGBO(0, 0, 0, 1),
                    fontWeight: FontWeight.w700,
                    fontSize: 24,
                    fontFamily: 'WorkSans'))),
        const SizedBox(height: 4),
        Padding(
          padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
          child: RichText(
              text: TextSpan(
                  text: bio,
                  style: const TextStyle(
                      color: Color.fromRGBO(0, 0, 0, 1),
                      fontWeight: FontWeight.w400,
                      fontSize: 15,
                      fontFamily: 'WorkSans'))),
        ),
        const SizedBox(height: 4),
        MaterialButton(
          child: Text(addFriendText,
              style: TextStyle(
                  fontSize: 14,
                  fontFamily: "WorkSans",
                  fontWeight: FontWeight.w700,
                  color: Color.fromRGBO(255, 255, 255, 1))),
          onPressed: () {
            _saveFriend();
          },
          color: addFriendColor,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(18.0)),
          minWidth: 70,
          height: 26,
        ),
      ],
    );
  }

  Widget buildWishListImage() => Container();
  Widget buildFriendsImage() => Container();

  Widget buildCoverImage() => Container(
        color: Colors.grey,
        child: CachedNetworkImage(
          imageUrl: BackPict,
          placeholder: (context, url) => Center(
            child: Transform.scale(
              scale: 0.5,
              child: const CircularProgressIndicator(),
            ),
          ),
          fit: BoxFit.fill,
          width: double.infinity,
          height: coverHeight,
          errorWidget: (context, url, error) =>
              const Center(child: Icon(Icons.error)),
        ),
      );

  Widget buildProfileImage() => CircleAvatar(
        radius: profileHeight / 2,
        backgroundColor: Colors.grey.shade800,
        backgroundImage: CachedNetworkImageProvider(PictUrl),
      );

  Future<List<GiftDataModel>> fetchGift() async {
    Dio dio = Dio();

    // final response =
    // await rootBundle.rootBundle.loadString('assets/gifts.json');
    final prefs = await SharedPreferences.getInstance();
    usernames = prefs.getString('user')!;
    friendy = widget.userFriend;
    //usernames = "username";
    print(usernames);
    var listwish;
    await dio
        .post(
            "https://us-central1-gifter-team26.cloudfunctions.net/getWishlistsByUserId",
            data: jsonEncode({"userId": friendy}))
        .then((value) => listwish = value.data);
    print("listwish: ${listwish[0]["gifts"]}");
    var listawish = listwish[0]["gifts"];
    List<GiftDataModel> gifts = [];
    GiftDataModel model;
    for (int i = 0; i < listawish.length; i++) {
      await dio
          .post("https://us-central1-gifter-team26.cloudfunctions.net/getGift",
              data: jsonEncode({"giftId": listawish[i]}))
          .then((value) {
        // var valor = value.data as GiftDataModel;
        model = GiftDataModel(
          id: value.data["id"],
          name: value.data["name"],
          description: value.data["description"],
          image: value.data["image"],
          price: value.data["price"].toDouble(),
          // categories: value.data["categories"]
        );
        gifts.add(model);
      });
    }
    return gifts;

    // final response = await http.get(Uri.parse(
    //     'https://us-central1-gifter-team26.cloudfunctions.net/getRecommendedGifts'));
    // if (response.statusCode == 200) {
    //   // If the server did return a 200 OK response,
    //   // then parse the JSON.
    //   final list = json.decode(response.body) as List<dynamic>;
    //   return list.map((e) => GiftDataModel.fromJson(e)).toList();
    // } else {
    //   // If the server did not return a 200 OK response,
    //   // then throw an exception.
    //   throw Exception('Failed to load Gift');
    // }
  }

  List<bool> isPressed = List.filled(9999, true);
  List<Color> heartColor = List.filled(9999, Colors.redAccent);

  Widget waterfallWidget() => FutureBuilder(
        future: fetch,
        builder: (context, data) {
          if (data.hasError) {
            print(data.data);
            return FallbackView2();
          } else if (data.hasData) {
            var items = data.data as List<GiftDataModel>;
            return Flexible(
                child: RefreshIndicator(
                    onRefresh: () {
                      return fetchGift2();
                    },
                    child: WaterfallFlow.builder(
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        padding: const EdgeInsets.symmetric(horizontal: 16),
                        itemCount: items.length,
                        gridDelegate:
                            const SliverWaterfallFlowDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          crossAxisSpacing: 8.0,
                          mainAxisSpacing: 8.0,
                        ),
                        itemBuilder: (context, index) {
                          // print("cat: ${items[index].stores!.runtimeType}");
                          return InkWell(
                              onTap: () async {
                                Isolate.spawn(calculateTime, 1);
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (_) => GiftDetail(
                                        items[index].id!,
                                        items[index].name!,
                                        items[index].image!,
                                        items[index].description!,
                                        items[index].price!,
                                        items[index].categories!,
                                        isPressed[index],
                                        stores: storesList),
                                  ),
                                );
                                registerFrequency("Gift Detail");
                              },
                              child: Stack(children: [
                                ClipRRect(
                                    borderRadius: BorderRadius.circular(10.0),
                                    child: CachedNetworkImage(
                                        imageUrl: items[index].image!,
                                        placeholder: (context, url) => Center(
                                                child: Transform.scale(
                                              scale: 0.5,
                                              child:
                                                  const CircularProgressIndicator(),
                                            )),
                                        errorWidget: (context, url, error) =>
                                            const Center(
                                                child: Icon(Icons.error)),
                                        color: const Color.fromRGBO(
                                            224, 224, 224, 1),
                                        colorBlendMode: BlendMode.modulate))
                              ]));
                        })));
          } else {
            return const Flexible(
                child: Center(
              child: CircularProgressIndicator(),
            ));
          }
        },
      );
}

Widget FallbackView2() {
  return Flexible(
      child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 25),
          // alignment: Alignment.center,
          child: Align(
              alignment: Alignment.center,
              child: Text("There are no gifts added to $friendy's wishlist",
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                      fontSize: 14,
                      fontFamily: "Montserrat",
                      fontWeight: FontWeight.w400)))));
}

Future<List<GiftDataModel>> fetchGift2() async {
  Dio dio = Dio();

  // var ids = giftIds;
  final prefs = await SharedPreferences.getInstance();
  usernames = prefs.getString('user')!;

  var ids;
  await dio
      .post(
          "https://us-central1-gifter-team26.cloudfunctions.net/getWishlistsByUserId",
          data: jsonEncode({"userId": friendy}))
      .then((value) => ids = value.data[0]["gifts"]);

  List<GiftDataModel> gifts = [];
  GiftDataModel model;
  for (int i = 0; i < ids.length; i++) {
    await dio
        .post("https://us-central1-gifter-team26.cloudfunctions.net/getGift",
            data: jsonEncode({"giftId": ids[i]}))
        .then((value) {
      // var valor = value.data as GiftDataModel;
      model = GiftDataModel(
          id: value.data["id"],
          name: value.data["name"],
          description: value.data["description"],
          image: value.data["image"],
          price: value.data["price"].toDouble(),
          categories: List<String>.from(value.data["categories"]).toList());
      print(
          "categories: ${List<String>.from(value.data["categories"]).toList().runtimeType}");
      gifts.add(model);
      print(model);
    });
  }
  return gifts;
}
