// ignore_for_file: non_constant_identifier_names

import 'dart:async';

import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:amplify_storage_s3/amplify_storage_s3.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:gifter/screens/frequency.dart';
import 'package:gifter/screens/profile.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../models/User.dart';
import 'fallbackView.dart';

import 'friendProfile.dart';

class Search extends StatefulWidget {
  const Search({Key? key}) : super(key: key);

  @override
  State<Search> createState() => _SearchState();
}

class _SearchState extends State<Search> {
  List<User> peopleList = [];
  List<User> visitedList = [];
  bool flag = false;
  Widget _appBarTitle = const Text(
    'Search People',
    style: TextStyle(color: Color.fromRGBO(246, 65, 108, 1)),
  );
  bool tapped = false;
  bool cancel = false;
  List<User> filteredList = [];
  Icon _searchIcon = const Icon(Icons.search);
  String _searchText = "";
  final TextEditingController _filter = TextEditingController();

  ConnectivityResult result = ConnectivityResult.none;
  late StreamSubscription subscription;

  _SearchState() {
    _filter.addListener(() {
      if (_filter.text.isEmpty) {
        //     setState(() {
        _searchText = "";
        filteredList = peopleList;
        //     });
      } else {
        //     setState(() {
        _searchText = _filter.text;
        //     });
      }
    });
  }

  List<String> profilePics = [];

  @override
  void initState() {
    super.initState();
    fetchPeople();
    profilePics = List.filled(99, "");
    Connectivity().checkConnectivity().then((value) {
      result = value;
    });
    subscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      setState(() => this.result = result);
      if (result != ConnectivityResult.none) {
        setState(() {
          ScaffoldMessenger.of(context).hideCurrentSnackBar();
        });
      } else {
        FallbackView(false, context);
      }
      // Got a new connectivity status!
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        centerTitle: true,
        title: _appBarTitle,
        leading: IconButton(
          icon: _searchIcon,
          color: const Color.fromRGBO(246, 65, 108, 1),
          onPressed: _searchPressed,
        ),
      ),
      body: Container(
          color: Colors.white,
          child: Column(
            children: [
              // SearchBar(),
              Results(), People()
            ],
          )),
    ));
  }

  Widget Results() {
    return const Padding(
        padding: EdgeInsets.all(16),
        child: Align(
          alignment: Alignment.centerLeft,
          child: Text(
              // flag ? 'Recently Visited People' : 'People',
              'People',
              style: TextStyle(
                  fontSize: 16,
                  fontFamily: "WorkSans",
                  fontWeight: FontWeight.w600,
                  color: Color.fromRGBO(23, 27, 26, 1))),
        ));
  }

  void fetchPeople() async {
    final prefs = await SharedPreferences.getInstance();
    var currentUser = prefs.getString('user');
    final users = await Amplify.DataStore.query(User.classType,
        where: User.USERNAME.ne(currentUser));
    // var visited = prefs.getStringList('visited');
    List<User> list = [];

    // if (visited != null) {
    //   for (int i = 0; i < visited.length; i++) {
    //     final user = await Amplify.DataStore.query(User.classType,
    //         where: User.USERNAME.eq(visited[i]));
    //     list.add(user[0]);
    //   }
    // }
    setState(() {
      peopleList = users;
      // visitedList = list;
      // flag = visited != null;
      filteredList = peopleList;
    });
  }

  void setVisitedUser(String user) async {
    final prefs = await SharedPreferences.getInstance();
    var temp = prefs.getStringList('visited');
    if (temp == null) {
      temp = [user];
    } else {
      temp.add(user);
    }
    prefs.setStringList('visited', temp);
  }

  void _searchPressed() {
    setState(() {
      if (_searchIcon.icon == Icons.search) {
        _searchIcon = const Icon(Icons.close);
        _appBarTitle = TextField(
          controller: _filter,
          cursorColor: const Color.fromRGBO(246, 65, 108, 1),
          decoration: const InputDecoration(
            filled: true,
            fillColor: Color.fromRGBO(230, 230, 230, 0.8),
            // prefixIcon: Icon(Icons.search),
            hintText: 'Search',
            border: OutlineInputBorder(
                borderSide: BorderSide.none,
                borderRadius: BorderRadius.all(Radius.circular(20))),
          ),
        );
      } else {
        _searchIcon = const Icon(Icons.search);
        _appBarTitle = const Text(
          'Search People',
          style: TextStyle(color: Color.fromRGBO(246, 65, 108, 1)),
        );
        filteredList = peopleList;
        _filter.clear();
      }
    });
  }

  Widget People() {
    fetchPeople();
    if (peopleList.isEmpty) {
      return Flexible(
          child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 45),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                // ignore: prefer_const_literals_to_create_immutables
                children: [
                  const Align(
                      alignment: Alignment.center,
                      child: Text("Oops! :(",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 20,
                              fontFamily: "WorkSans",
                              fontWeight: FontWeight.w800))),
                  const Padding(padding: EdgeInsets.only(bottom: 16)),
                  const Align(
                    alignment: Alignment.center,
                    child: Text(
                      "There are no users yet or you don't have connection. Please comeback later.",
                      textAlign: TextAlign.center,
                      style: TextStyle(fontFamily: "WorkSans", fontSize: 16),
                    ),
                  )
                ],
              )));
    }

    if (_filter.text.isEmpty) {
      setState(() {
        _searchText = "";
        filteredList = peopleList;
      });
    } else {
      setState(() {
        _searchText = _filter.text;
      });
    }

    if (_searchText.isNotEmpty) {
      List<User> tempList = [];
      for (int i = 0; i < filteredList.length; i++) {
        if (filteredList[i]
            .name
            .toLowerCase()
            .contains(_searchText.toLowerCase())) {
          tempList.add(filteredList[i]);
        }
      }
      filteredList = tempList;
    }
    // else if (visitedList.isNotEmpty) {
    //   filteredList = visitedList;
    // }
    // var list = visitedList.isNotEmpty && _searchText.isEmpty ? filteredList : visitedList;
    if (filteredList.isEmpty) {
      return const Flexible(
          child: Padding(
              padding: EdgeInsets.all(16),
              child: Align(
                alignment: Alignment.center,
                child: Text(
                    "Oops! We didn't found people that matched your search :(",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontFamily: "WorkSans", fontSize: 16)),
              )));
    }
    // if (profilePics == List.filled(99, "")) {
    setProfilePic(filteredList);
    // }

    return Flexible(
        child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: ListView.builder(
                itemCount: filteredList.length,
                // scrollDirection: Axis.vertical,
                itemBuilder: (context, index) {
                  // setProfilePic(index, filteredList[index].profilePic!);
                  // print("profilePics: ${profilePics[index]}");
                  return Column(children: [
                    Padding(
                      padding: const EdgeInsets.only(bottom: 16),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(children: [
                            // User's profile pic
                            profilePics[index] == ""
                                ? const Image(
                                    image: AssetImage("assets/profileIcon.png"),
                                    height: 60,
                                    width: 60,
                                  )
                                : CachedNetworkImage(
                                    imageUrl: profilePics[index],
                                    imageBuilder: (context, imageProvider) =>
                                        Container(
                                          width: 60.0,
                                          height: 60.0,
                                          decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            image: DecorationImage(
                                                image: imageProvider,
                                                fit: BoxFit.cover),
                                          ),
                                        ),
                                    memCacheWidth: 80,
                                    memCacheHeight: 80,
                                    placeholder: (context, url) => Center(
                                        child:
                                            // Transform.scale(
                                            //   scale: 0.5,
                                            //   child:
                                            //       const CircularProgressIndicator(),
                                            // ),
                                            Container(
                                                width: 60.0,
                                                height: 60.0,
                                                decoration: const BoxDecoration(
                                                    shape: BoxShape.circle,
                                                    color: Colors.grey))),
                                    fit: BoxFit.fill,
                                    errorWidget: (context, url, error) =>
                                        Container(
                                            width: 60.0,
                                            height: 60.0,
                                            decoration: const BoxDecoration(
                                                shape: BoxShape.circle,
                                                color: Colors.grey))),
                            // User's name
                            Padding(
                              padding: const EdgeInsets.only(left: 16),
                              child: SizedBox(
                                  width: 150,
                                  child: Align(
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      filteredList[index].name,
                                      style: const TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400,
                                          fontFamily: "WorkSans"),
                                    ),
                                  )),
                            ),
                          ]),
                          InkWell(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (_) => friendProfile(context,
                                        userFriend:
                                            filteredList[index].username,
                                        donde: 0)),
                              );
                              setVisitedUser(filteredList[index].username);
                              registerFrequency("Another Profile");
                            },
                            child: const Align(
                                alignment: Alignment.centerRight,
                                child: Icon(Icons.arrow_forward,
                                    color: Color.fromRGBO(0, 0, 0, 0.5))),
                          )
                        ],
                      ),
                    ),
                  ]);
                })));
  }

  Future<void> setProfilePic(List<User> users) async {
    var pic;
    List<String> list = List.filled(99, "");
    for (int i = 0; i < users.length; i++) {
      pic = users[i].profilePic;
      if (pic != "") {
        GetUrlResult urlpic = await Amplify.Storage.getUrl(key: pic);
        // setState(() {
        list[i] = urlpic.url;
        // });
      }
    }
    profilePics = list;
  }

  Widget SearchBar() {
    return Container(
        color: const Color.fromRGBO(230, 230, 230, 1),
        padding: const EdgeInsets.all(16),
        child: Row(
          children: [
            Expanded(
                child: TextField(
                    onTap: () {
                      setState(() {
                        tapped = true;
                        cancel = true;
                      });
                    },
                    onEditingComplete: () {
                      setState(() {
                        tapped = false;
                        cancel = false;
                      });
                    },
                    controller: _filter,
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.white,
                      prefixIcon: Icon(Icons.search,
                          color: tapped
                              ? const Color.fromRGBO(246, 65, 108, 1)
                              : Colors.grey),
                      hintText: 'Search',
                      hintStyle:
                          const TextStyle(fontFamily: "WorkSans", fontSize: 14),
                      labelStyle:
                          const TextStyle(fontFamily: "WorkSans", fontSize: 14),
                      border: const OutlineInputBorder(
                          borderSide: BorderSide.none,
                          borderRadius: BorderRadius.all(Radius.circular(20))),
                    ))),
            Padding(padding: EdgeInsets.only(right: cancel ? 8 : 0)),
            Align(
                alignment: Alignment.center,
                child: InkWell(
                    onTap: () {
                      setState(() {
                        cancel = false;
                      });
                    },
                    child: Text(
                      cancel ? "Cancel" : "",
                      style: const TextStyle(
                          fontSize: 14,
                          color: Color.fromRGBO(246, 65, 108, 1),
                          fontFamily: "WorkSans"),
                    )))
          ],
        ));
  }
}
