// ignore_for_file: non_constant_identifier_names, file_names

import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:isolate';
import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
// ignore: library_prefixes
import 'package:geolocator/geolocator.dart';
import 'package:gifter/screens/giftDataModel.dart';
import 'package:gifter/screens/storeDetail.dart';
import 'package:path_provider/path_provider.dart';
import '../models/Tiempo.dart';
import './StoreDataModel.dart';
import 'package:http/http.dart' as http;

import 'fallbackView.dart';
import 'frequency.dart';

class ListStores extends StatefulWidget {
  const ListStores({Key? key}) : super(key: key);

  @override
  _ListStoresState createState() => _ListStoresState();
}

var gfts;

class _ListStoresState extends State<ListStores> {
  double getTimeNow() {
    String now = DateTime.now().toString();
    String time = now.split(" ")[1];
    var arraytime = time.split(":");
    int minute = int.parse(arraytime[1]);
    int hour = int.parse(arraytime[0]);
    double seconds = double.parse(arraytime[2]);
    double timeSeconds = minute * 60 + seconds + hour * 60 * 60;
    return timeSeconds;
  }

  var fetch;

  double inicio = 0;

  late int _selectedIndex;
  late List<Widget> _pages;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: Column(
        children: [GifterLogo, StoresHeader, StoresInfo],
      ),
      // bottomNavigationBar: buildNavigation(),
    ));
  }

  Widget get GifterLogo {
    return Container(
        padding: const EdgeInsets.only(left: 16, top: 16, bottom: 16),
        child: Row(
          // ignore: prefer_const_literals_to_create_immutables
          children: [
            const Image(
              image: AssetImage("assets/logo.png"),
              height: 27,
            ),
            const Padding(padding: EdgeInsets.only(right: 4)),
            const Text('Gifter',
                style: TextStyle(
                    fontSize: 18,
                    fontFamily: 'PTSerif',
                    fontWeight: FontWeight.w700,
                    color: Color.fromRGBO(23, 27, 26, 1)))
          ],
        ));
  }

  Widget get StoresHeader {
    return Container(
        padding: const EdgeInsets.only(left: 16, bottom: 24),
        // ignore: prefer_const_literals_to_create_immutables
        child: Row(children: [
          const Text('Take a look at gift shops near you',
              style: TextStyle(
                  fontSize: 16,
                  fontFamily: "Montserrat",
                  fontWeight: FontWeight.w600,
                  color: Color.fromRGBO(23, 27, 26, 1)))
        ]));
  }

  final ScrollController _scrollController = ScrollController();
  // List<Future<StoreDataModel>> myList = [];
  late Future<List<StoreDataModel>> storesList;

  ConnectivityResult result = ConnectivityResult.none;
  late StreamSubscription subscription;

  @override
  void initState() {
    super.initState();
    inicio = getTimeNow();
    storesList = ReadJsonData();
    // registerFrequency("Stores List");
    Connectivity().checkConnectivity().then((value) {
      result = value;
    });
    subscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      setState(() => this.result = result);
      if (result == ConnectivityResult.none) {
        setState(() => _showSnackBar());
      } else {
        setState(() => ScaffoldMessenger.of(context).hideCurrentSnackBar());
      }
      // Got a new connectivity status!
    });
  }

  dispose() {
    super.dispose();

    subscription.cancel();
  }

  void _showSnackBar() {
    ScaffoldMessenger.of(context).hideCurrentSnackBar();

    SnackBar snackBar = const SnackBar(
        content: Text("There is no internet connection"),
        duration: Duration(days: 1));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  // _getMoreData() {
  //   for (int i = _currentMax; i < _currentMax + 5; i++) {
  //     myList.add(orden(ReadJsonData()).then((value) => value.elementAt(i)));
  //   }

  //   _currentMax += 5;
  //   print(_currentMax);

  //   setState(() {});
  // }

  Widget get StoresInfo {
    return FutureBuilder(
        // future: orden(ReadJsonData()),
        future: storesList,
        builder: (context, data) {
          if (data.hasError) {
            return Text("data.error: ${data.error}");
            // return FallbackView(false, context);
          } else if (data.hasData) {
            var items = data.data as List<StoreDataModel>;
            return Flexible(
                child: RefreshIndicator(
                    onRefresh: () {
                      return ReadJsonData();
                    },
                    child: ListView.builder(
                        // controller: _scrollController,
                        scrollDirection: Axis.vertical,
                        padding: const EdgeInsets.symmetric(horizontal: 16),
                        itemCount: items.length,
                        itemBuilder: (context, index) {
                          // gfts = storeGifts(index);
                          return Column(
                            children: [
                              // Initial store info
                              Padding(
                                padding: const EdgeInsets.only(bottom: 16),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(children: [
                                      // Store circular image
                                      CachedNetworkImage(
                                        imageUrl:
                                            items[index].images.toString(),
                                        imageBuilder:
                                            (context, imageProvider) =>
                                                Container(
                                          width: 80.0,
                                          height: 80.0,
                                          decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            image: DecorationImage(
                                                image: imageProvider,
                                                fit: BoxFit.cover),
                                          ),
                                        ),
                                        placeholder: (context, url) => Center(
                                          child: Transform.scale(
                                            scale: 0.5,
                                            child:
                                                const CircularProgressIndicator(),
                                          ),
                                        ),
                                        fit: BoxFit.fill,
                                        errorWidget: (context, url, error) =>
                                            const Center(
                                                child: Icon(Icons.error)),
                                      ),
                                      // Store title and description
                                      Padding(
                                          padding:
                                              const EdgeInsets.only(left: 16),
                                          child: SizedBox(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width /
                                                2.5,
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  items[index].name.toString(),
                                                  style: const TextStyle(
                                                      fontSize: 16,
                                                      fontWeight:
                                                          FontWeight.w600,
                                                      fontFamily: "Montserrat"),
                                                ),
                                                const Padding(
                                                    padding: EdgeInsets.only(
                                                        bottom: 4)),
                                                Text(
                                                  items[index]
                                                          .description
                                                          .toString()
                                                          .substring(0, 30) +
                                                      "...",
                                                  style: const TextStyle(
                                                      fontSize: 14,
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      fontFamily: "WorkSans"),
                                                )
                                              ],
                                            ),
                                          )),
                                    ]),
                                    InkWell(
                                        onTap: () async {
                                          double last = getTimeNow();
                                          double tiempoStay = last - inicio;
                                          try {
                                            final tiemporegister = Tiempo(
                                                name: "Stores List",
                                                time: tiempoStay);
                                            await Amplify.DataStore.save(
                                                tiemporegister);
                                          } catch (e) {
                                            print(e);
                                          }
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                              builder: (_) => StoreDetail(
                                                items[index].name,
                                                items[index].description,
                                                items[index].images,
                                                items[index].storeLoc,
                                                items[index].website,
                                                items[index].phone,
                                                items[index].address,
                                                index,
                                                gifts: gfts,
                                              ),
                                            ),
                                          );
                                          registerFrequency("Store Detail");
                                        },
                                        child: const Text("View shop",
                                            style: TextStyle(
                                                fontSize: 14,
                                                fontWeight: FontWeight.w600,
                                                fontFamily: "WorkSans",
                                                color: Color.fromRGBO(
                                                    246, 65, 108, 1)))),
                                  ],
                                ),
                              ),
                              FutureBuilder(
                                  future: storeGifts(index),
                                  builder: (context, data) {
                                    if (data.hasError) {
                                      print("dataerror: ${data.data}");
                                      return const Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 16),
                                          child: Text(
                                            "We couldn't show the store gifts. Please check your connection and try again.",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontFamily: "WorkSans",
                                                color: Colors.blueGrey,
                                                fontSize: 14),
                                          ));
                                    } else if (data.hasData) {
                                      gfts = data.data as List<GiftDataModel>;
                                      return SizedBox(
                                          height: 120.0,
                                          child: ListView.builder(
                                              scrollDirection: Axis.horizontal,
                                              itemCount: gfts.length,
                                              itemBuilder: (context, index) {
                                                return Container(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            right: 8,
                                                            bottom: 24),
                                                    child: ClipRRect(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(7),
                                                        child:
                                                            CachedNetworkImage(
                                                          imageUrl: gfts[index]
                                                              .image
                                                              .toString(),
                                                          height: 120,
                                                          placeholder:
                                                              (context, url) =>
                                                                  Center(
                                                            child:
                                                                Transform.scale(
                                                              scale: 0.5,
                                                              child:
                                                                  const CircularProgressIndicator(),
                                                            ),
                                                          ),
                                                          fit: BoxFit.fill,
                                                          errorWidget: (context,
                                                                  url, error) =>
                                                              const Center(
                                                                  child: Icon(Icons
                                                                      .error)),
                                                        )));
                                              }));
                                    } else {
                                      // return const Flexible(
                                      return Center(
                                          child: Transform.scale(
                                              scale: 0.5,
                                              child:
                                                  CircularProgressIndicator()));
                                      // return Text("");
                                    }
                                  }),
                              const Padding(padding: EdgeInsets.only(bottom: 8))
                            ],
                          );
                        })));
          } else {
            return const Flexible(
                child: Center(
              child: CircularProgressIndicator(),
            ));
          }
        });
  }
}

// Future<List<StoreDataModel>> orden(Future<List<StoreDataModel>> item) async {
//   // print(item);
//   // List<StoreDataModel> retorno = await item;
//   List<StoreDataModel> r = await item;
//   double? lat = 0.0;
//   double? long = 0.0;
//   // ignore: unused_local_variable
//   double distancia = 9999999999999.0;
//   double distanciacomp = 0;

//   Position localizacion = await _getGeoLocationPosition();
//   double currentlat = localizacion.latitude;
//   double currentlong = localizacion.longitude;

//   List ordenamiento = [];
//   List ord = [];
//   // print("-----");
//   // print("aaaa");
//   // print(currentlat);
//   // print(currentlong);
//   // print("aaa");
//   for (var i = 0; i < r.length; i++) {
//     long = r[i].location!.latitude;
//     lat = r[i].location!.longitude;

//     distanciacomp =
//         //Geolocator.distanceBetween(lat!, long!, currentlat, currentlong);
//         acos(sin(lat * 3.14 / 180) * sin(currentlat * 3.14 / 180) +
//                 cos(lat * 3.14 / 180) *
//                     cos(currentlat * 3.14 / 180) *
//                     cos(currentlong * 3.14 / 180 - long * 3.14 / 180)) *
//             6371000;

//     distancia = distanciacomp;

//     ordenamiento.add([distanciacomp, r[i].name]);
//     ord.add(distanciacomp);
//   }

//   ord.sort();
//   // print(ordenamiento);
//   List<StoreDataModel> revisar = [];
//   List<int> mirar = [];
//   for (var i = 0; i < ordenamiento.length; i++) {
//     for (var j = 0; j < ordenamiento.length; j++) {
//       if (ord[i] == ordenamiento[j][0]) {
//         revisar.add(r[j]);
//         mirar.add(j);
//       }
//     }
//   }
//   // print(mirar);
//   return revisar;
// }

Future<Position> _getGeoLocationPosition() async {
  bool serviceEnabled;
  LocationPermission permission;
  // Test if location services are enabled.
  serviceEnabled = await Geolocator.isLocationServiceEnabled();
  if (!serviceEnabled) {
    // Location services are not enabled don't continue
    // accessing the position and request users of the
    // App to enable the location services.
    await Geolocator.openLocationSettings();
    return Future.error('Location services are disabled.');
  }
  permission = await Geolocator.checkPermission();
  if (permission == LocationPermission.denied) {
    permission = await Geolocator.requestPermission();
    if (permission == LocationPermission.denied) {
      return Future.error('Location permissions are denied');
    }
  }
  if (permission == LocationPermission.deniedForever) {
    // Permissions are denied forever, handle appropriately.
    return Future.error(
        'Location permissions are permanently denied, we cannot request permissions.');
  }
  // When we reach here, permissions are granted and we can
  // continue accessing the position of the device.
  return await Geolocator.getCurrentPosition();
}

Future<List<StoreDataModel>> ReadJsonData() async {
  // final jsondata = await rootBundle.rootBundle.loadString('assets/stores.json');
  ReceivePort port = ReceivePort();

  String fileName = "stores.json";
  var dir = await getTemporaryDirectory();

  File file = File(dir.path + "/" + fileName);

  if (file.existsSync()) {
    // file.delete(recursive: false);
    print("Reading stores from device cache");

    final data = file.readAsStringSync();
    final list = jsonDecode(data) as List<dynamic>;
    return list.map((e) => StoreDataModel.fromJson(e)).toList();
  } else {
    print("Fetching stores from the network");
    String url =
        'https://us-central1-gifter-team26.cloudfunctions.net/getNearbyStores';
    dynamic lat, long;
    Position geo;

    try {
      geo = await _getGeoLocationPosition();
      lat = geo.latitude;
      long = geo.longitude;
    } catch (e) {
      lat = null;
      long = null;
    }

    final http.Response response;

    if (lat != null && long != null) {
      response = await http.post(Uri.parse(url),
          body: jsonEncode({
            "location": {"latitude": lat, "longitude": long}
          }));
    } else {
      response = await http.post(Uri.parse(url));
    }

    if (response.statusCode == 200) {
      file.writeAsStringSync(response.body, flush: true, mode: FileMode.write);
      final isolate = await Isolate.spawn<List<dynamic>>(
          _decodeAndParseStores, [port.sendPort, response.body]);
      final stores = await port.first;
      isolate.kill(priority: Isolate.immediate);
      return stores;
    } else {
      throw Exception('Failed to load Stores');
    }
  }
}

void _decodeAndParseStores(List<dynamic> values) {
  SendPort sendPort = values[0];
  String data = values[1];
  final list = json.decode(data) as List<dynamic>;
  sendPort.send(list.map((e) => StoreDataModel.fromJson(e)).toList());
}

Future<List<GiftDataModel>> storeGifts(int index) async {
  Dio dio = Dio();
  var gifts = [];
  List<GiftDataModel> giftsList = [];
  var model;
  final res = await dio.post(
      "https://us-central1-gifter-team26.cloudfunctions.net/getStoreGifts",
      data: jsonEncode({"storeId": index}));
  gifts = res.data;
  for (int i = 0; i < 5; i++) {
    model = GiftDataModel(
        id: gifts[i]["id"],
        name: gifts[i]["name"],
        description: gifts[i]["description"],
        image: gifts[i]["image"],
        price: gifts[i]["price"].toDouble(),
        categories: List<String>.from(gifts[i]["categories"]).toList());
    // stores: gifts[i]["stores"]);
    giftsList.add(model);
  }
  return giftsList;
}
