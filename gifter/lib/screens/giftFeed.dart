// ignore_for_file: file_names, non_constant_identifier_names, must_be_immutable

import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:isolate';

import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
// ignore: library_prefixes
import 'package:gifter/screens/StoreDataModel.dart';
import 'package:gifter/screens/giftDetail.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:waterfall_flow/waterfall_flow.dart';
import 'package:http/http.dart' as http;

import '../models/Tiempo.dart';
import 'fallbackView.dart';
import 'frequency.dart';
import 'giftDataModel.dart';

List<String> giftIds = [];

Future<List<GiftDataModel>> fetchGift() async {
  ReceivePort port = ReceivePort();

  String fileName = "gifts.json";
  var dir = await getTemporaryDirectory();
  File file = File(dir.path + "/" + fileName);

  final isolate;
  final stores;

  if (file.existsSync()) {
    print("Reading gifts from device cache");

    final data = file.readAsStringSync();
    isolate = await Isolate.spawn<List<dynamic>>(
        _decodeAndParseGifts, [port.sendPort, data]);
    stores = await port.first;
    isolate.kill(priority: Isolate.immediate);

    return stores;
  } else {
    print("Fetching gifts from the network");

    final response = await http.post(Uri.parse(
        "https://us-central1-gifter-team26.cloudfunctions.net/getRecommendedGifts"));

    if (response.statusCode == 200) {
      file.writeAsStringSync(response.body, flush: true, mode: FileMode.write);
      isolate = await Isolate.spawn<List<dynamic>>(
          _decodeAndParseGifts, [port.sendPort, response.body]);
      stores = await port.first;
      isolate.kill(priority: Isolate.immediate);

      return stores;
    } else {
      return json.decode(response.body);
    }
  }
}

void _decodeAndParseGifts(List<dynamic> values) {
  SendPort sendPort = values[0];
  String data = values[1];
  final list = json.decode(data) as List<dynamic>;
  sendPort.send(list.map((e) => GiftDataModel.fromJson(e)).toList());
}

class GiftFeed extends StatefulWidget with ChangeNotifier {
  GiftFeed({Key? key}) : super(key: key);

  @override
  _GiftFeedState createState() => _GiftFeedState();
}

class _GiftFeedState extends State<GiftFeed> {
  List<bool> isPressed = List.filled(99, false);
  List<Color> heartColor = List.filled(99, Colors.white);

  late Future<List<GiftDataModel>> futureGifts;
  double getTimeNow() {
    String now = DateTime.now().toString();
    String time = now.split(" ")[1];
    var arraytime = time.split(":");
    int minute = int.parse(arraytime[1]);
    int hour = int.parse(arraytime[0]);
    double seconds = double.parse(arraytime[2]);
    double timeSeconds = minute * 60 + seconds + hour * 60 * 60;
    return timeSeconds;
  }

  double inicio = 0;
  int sum = 0;

  ConnectivityResult result = ConnectivityResult.none;
  late StreamSubscription subscription;

  @override
  void initState() {
    super.initState();
    inicio = getTimeNow();
    futureGifts = fetchGift();
    // registerFrequency("Gift Feed");
    Connectivity().checkConnectivity().then((value) {
      result = value;
    });
    subscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      setState(() => this.result = result);
      if (result == ConnectivityResult.none) {
        setState(() => FallbackView(false, context));
      } else {
        setState(() => ScaffoldMessenger.of(context).hideCurrentSnackBar());
        // futureGifts = fetchGift();
      }
      // Got a new connectivity status!
    });
  }

  @override
  dispose() {
    super.dispose();

    subscription.cancel();
  }

  void changeColor(int i, bool click, String giftId) {
    setState(() {
      isPressed[i] = click;
      heartColor[i] = isPressed[i] ? Colors.redAccent : Colors.white;
    });
  }

  var storesList = [
    StoreDataModel(
        name: "Name",
        images:
            "https://grownandflown.com/wp-content/uploads/2017/12/gifts-.jpeg",
        description: "Description",
        website: "",
        phone: "000",
        address: "",
        // location: GeoPoint(latitude: 1.2, longitude: 2.2),
        gifts: []),
    StoreDataModel(
        name: "Name",
        images:
            "https://grownandflown.com/wp-content/uploads/2017/12/gifts-.jpeg",
        description: "Description",
        website: "",
        phone: "000",
        address: "",
        // location: GeoPoint(latitude: 1.2, longitude: 2.2),
        gifts: []),
    StoreDataModel(
        name: "Name",
        images:
            "https://grownandflown.com/wp-content/uploads/2017/12/gifts-.jpeg",
        description: "Description",
        website: "",
        phone: "000",
        address: "",
        // location: GeoPoint(latitude: 1.2, longitude: 2.2),
        gifts: [])
  ];

  @override
  Widget build(BuildContext context) {
    // if (result == ConnectivityResult.none) {
    //   return FallbackView(false, context);
    // }
    return FutureBuilder(
      future: futureGifts,
      builder: (context, data) {
        if (data.hasError) {
          // var s = data as List<GiftDataModel>;
          print("data.data: ${data}");
          return FallbackView(false, context);
        } else if (data.hasData) {
          var items = data.data as List<GiftDataModel>;
          return Flexible(
              child: RefreshIndicator(
                  onRefresh: () {
                    return fetchGift();
                  },
                  child: WaterfallFlow.builder(
                      scrollDirection: Axis.vertical,
                      cacheExtent: 15,
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      itemCount: items.length,
                      //itemCount: 2,
                      gridDelegate:
                          const SliverWaterfallFlowDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        crossAxisSpacing: 8.0,
                        mainAxisSpacing: 8.0,
                        // collectGarbage: (List<int> garbages) {
                        //   ///collectGarbage
                        //   garbages.forEach((index) {
                        //     final provider = ExtendedNetworkImageProvider(
                        //       items[index].image,
                        //     );
                        //     provider.evict();
                        //   });
                        // },
                      ),
                      itemBuilder: (context, index) {
                        pressed(items[index].id!).then((value) {
                          setState(() {
                            isPressed[index] = value;
                            heartColor[index] = isPressed[index]
                                ? Colors.redAccent
                                : Colors.white;
                          });
                        });

                        return InkWell(
                            onTap: () async {
                              double last = getTimeNow();
                              double tiempoStay = last - inicio;
                              try {
                                final tiemporegister =
                                    Tiempo(name: "Gift Feed", time: tiempoStay);
                                await Amplify.DataStore.save(tiemporegister);
                              } catch (e) {
                                print(e);
                              }
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (_) => GiftDetail(
                                      items[index].id!,
                                      items[index].name!,
                                      items[index].image!,
                                      items[index].description!,
                                      // items[index].stores!,
                                      items[index].price!,
                                      items[index].categories!,
                                      isPressed[index],
                                      stores: storesList),
                                ),
                              );
                              registerFrequency("Gift Detail");
                            },
                            child: Stack(children: [
                              ClipRRect(
                                  borderRadius: BorderRadius.circular(10.0),
                                  child: CachedNetworkImage(
                                      imageUrl: items[index].image!,
                                      placeholder: (context, url) => Center(
                                              child: Transform.scale(
                                            scale: 0.5,
                                            child:
                                                const CircularProgressIndicator(),
                                          )),
                                      errorWidget: (context, url, error) =>
                                          const Center(
                                              child: Icon(Icons.error)),
                                      color: const Color.fromRGBO(
                                          224, 224, 224, 1),
                                      colorBlendMode: BlendMode.modulate)),
                              Align(
                                  alignment: Alignment.topRight,
                                  child: IconButton(
                                    splashRadius: 10,
                                    iconSize: 24,
                                    icon: Icon(Icons.favorite,
                                        color: heartColor[index]),
                                    onPressed: () {
                                      setState(() {
                                        isPressed[index] = !isPressed[index];
                                        heartColor[index] = isPressed[index]
                                            ? Colors.redAccent
                                            : Colors.white;
                                      });
                                      saveToWishlist(
                                          isPressed[index], items[index].id!);
                                    },
                                  ))
                            ]));
                      })));
        } else {
          return const Flexible(
              child: Center(
            child: CircularProgressIndicator(),
          ));
        }
      },
    );
  }

  void saveToList(bool save, String giftId) {
    setState(() {
      save ? giftIds.add(giftId) : giftIds.remove(giftId);
    });
  }
}

Future<bool> pressed(String index) async {
  final prefs = await SharedPreferences.getInstance();
  var user = prefs.getString('user') ?? "";
  var list = prefs.getStringList("${user}Wishlist") ?? [];

  return list.contains(index.toString());
}

// Future<void> saveToWishlist(bool save, String giftId) async {
//   Dio dio = Dio();

//   bool connection = await InternetConnectionChecker().hasConnection;
//   var awsUsername = await Amplify.Auth.getCurrentUser();

//   if (connection) {
//     if (save) {
//       // giftIds.add(giftId);
//       final response = await dio.post(
//         "https://us-central1-gifter-team26.cloudfunctions.net/addGiftToWishlist",
//         data: jsonEncode(
//             <String, String>{"userId": awsUsername.username, "giftId": giftId}),
//       );
//     } else {
//       // giftIds.remove(giftId);
//       final response = await dio.post(
//         "https://us-central1-gifter-team26.cloudfunctions.net/removeGiftFromWishlist",
//         data: jsonEncode(
//             <String, String>{"userId": awsUsername.username, "giftId": giftId}),
//       );
//     }
//   }
//   // notifyListeners();
// }

void saveToWishlist(bool save, String giftId) async {
  final prefs = await SharedPreferences.getInstance();
  var user = prefs.getString('user') ?? "";
  if (user != "") {
    var list = prefs.getStringList("${user}Wishlist") ?? [];

    save ? list.add(giftId) : list.remove(giftId);
    prefs.setStringList("${user}Wishlist", list);
  }
}
