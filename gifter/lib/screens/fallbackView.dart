// ignore_for_file: non_constant_identifier_names, file_names
import 'friendProfile.dart';
import 'package:flutter/material.dart';

Widget FallbackView(bool small, BuildContext context) {
  double size = MediaQuery.of(context).size.height;
  return Flexible(
      child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 55),
          // alignment: Alignment.center,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            // ignore: prefer_const_literals_to_create_immutables
            children: [
              Icon(
                Icons.wifi_off,
                size: small ? size / 10 : size / 7,
                color: Colors.grey,
              ),
              const Padding(padding: EdgeInsets.only(bottom: 16)),
              const Align(
                  alignment: Alignment.center,
                  child: Text("Oops!",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 18,
                          fontFamily: "Montserrat",
                          fontWeight: FontWeight.w600))),
              const Padding(padding: EdgeInsets.only(bottom: 16)),
              const Align(
                  alignment: Alignment.center,
                  child: Text(
                      "There was an error trying to show the data. Please check your internet connection and try again.",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 15,
                          fontFamily: "Montserrat",
                          fontWeight: FontWeight.w400)))
            ],
          )));
}
