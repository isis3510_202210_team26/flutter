import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../models/SectionsFrequency.dart';

void registerFrequency(String screen) async {
  try {
    final prefs = await SharedPreferences.getInstance();
    prefs.setDouble(screen, (prefs.getDouble(screen) ?? 0) + 1);
    SectionsFrequency frequency = SectionsFrequency(
        sections: screen,
        sumFrequency: prefs.getDouble(screen) ?? 0,
        avgFrequency: 1);
    await Amplify.DataStore.save(frequency);
  } catch (e) {
    print(e);
  }
}
