import 'package:flutter/material.dart';
// import 'package:custom_navigator/custom_navigator.dart';
// import 'package:custom_navigator/custom_scaffold.dart';
import 'package:gifter/screens/bottom_navigation_bar/tab_navigator.dart';

import '../frequency.dart';

class BottomNavBar extends StatefulWidget {
  const BottomNavBar({Key? key}) : super(key: key);

  @override
  State<BottomNavBar> createState() => _BottomNavBarState();
}

class _BottomNavBarState extends State<BottomNavBar> {
  String _currentPage = "Feed";

  int _selectedIndex = 0;

  List<String> pageKeys = ["Feed", "Stores", "Explore", "Profile", "Menu"];

  // List pages = [GoodFeed(), ListStores(), ListStores()];

  GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  final Map<String, GlobalKey<NavigatorState>> _navigatorKeys = {
    "Feed": GlobalKey<NavigatorState>(),
    "Stores": GlobalKey<NavigatorState>(),
    "Explore": GlobalKey<NavigatorState>(),
    "Profile": GlobalKey<NavigatorState>(),
    "Menu": GlobalKey<NavigatorState>(),
  };

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        final isFirstRouteInCurrentTab =
            !await _navigatorKeys[_currentPage]!.currentState!.maybePop();
        if (isFirstRouteInCurrentTab) {
          if (_currentPage != "Feed") {
            _selectTab("Feed", 1);
            return false;
          }
        }
        return isFirstRouteInCurrentTab;
      },
      child: Scaffold(
        body: Stack(children: <Widget>[
          _buildOffStageNavigator("Feed"),
          _buildOffStageNavigator("Stores"),
          _buildOffStageNavigator("Explore"),
          _buildOffStageNavigator("Profile"),
          _buildOffStageNavigator("Menu"),
        ]),
        bottomNavigationBar: BottomNavigationBar(
          onTap: (int index) {
            _selectTab(pageKeys[index], index);
          },
          currentIndex: _selectedIndex,
          selectedItemColor: const Color.fromRGBO(246, 65, 108, 1),
          unselectedItemColor: Colors.grey,
          showUnselectedLabels: true,
          type: BottomNavigationBarType.fixed,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Feed'),
            BottomNavigationBarItem(
                icon: Icon(Icons.storefront), label: 'Stores'),
            BottomNavigationBarItem(icon: Icon(Icons.search), label: 'Explore'),
            BottomNavigationBarItem(icon: Icon(Icons.person), label: 'Profile'),
            BottomNavigationBarItem(icon: Icon(Icons.menu), label: 'Menu'),
          ],
        ),
      ),
    );
  }

  void _selectTab(String tabItem, int index) {
    // if (tabItem == _currentPage) {
    //   _navigatorKeys[tabItem]!.currentState?.popUntil((route) => route.isFirst);
    // } else {
    setState(() {
      _currentPage = pageKeys[index];
      _selectedIndex = index;
    });
    registerFrequency(tabItem);
  }

  Widget _buildOffStageNavigator(String tabItem) {
    return Offstage(
        offstage: _currentPage != tabItem,
        child: TabNavigator(
          navigatorKey: _navigatorKeys[tabItem]!,
          tabItem: tabItem,
        ));
  }
}
