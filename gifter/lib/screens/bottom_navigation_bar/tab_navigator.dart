import 'package:flutter/material.dart';
import 'package:gifter/screens/burgerMenu.dart';
import 'package:gifter/screens/goodFeed.dart';
import 'package:gifter/screens/listStores.dart';
import 'package:gifter/screens/profile.dart';
import 'package:gifter/screens/search.dart';

class TabNavigator extends StatelessWidget {
  final GlobalKey<NavigatorState> navigatorKey;
  final String tabItem;

  const TabNavigator(
      {Key? key, required this.navigatorKey, required this.tabItem})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    late Widget child;

    if (tabItem == "Feed") {
      child = const GoodFeed();
    } else if (tabItem == "Stores") {
      child = const ListStores();
    } else if (tabItem == "Explore") {
      child = const Search();
    } else if (tabItem == "Profile") {
      child = profile(context, donde: 0);
    } else if (tabItem == "Menu") {
      child = const BurgerMenu();
    }

    return Navigator(
      key: navigatorKey,
      onGenerateRoute: (routeSettings) {
        return MaterialPageRoute(builder: (context) => child);
      },
    );
  }
}
