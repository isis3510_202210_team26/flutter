// ignore_for_file: file_names

import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:gifter/models/ModelProvider.dart';
import 'package:shared_preferences/shared_preferences.dart';

double startTime = 0;
double endTime = 0;

void setStart(double start) {
  startTime = start;
}

void setEnd(double end) {
  endTime = end;
}

Future<void> sessionTime() async {
  if (startTime != 0 && endTime != 0) {
    double sessionTime = endTime - startTime;
    final prefs = await SharedPreferences.getInstance();
    var st = prefs.getStringList('sessionTimes') ?? [];
    st.add(sessionTime.toString());
    prefs.setStringList('sessionTimes', st);

    double sum = 0;
    for (int i = 0; i < st.length; i++) {
      sum += double.parse(st[i]);
    }
    AvgSessionTime avgSessionTime = AvgSessionTime(avg: sum / st.length);
    await Amplify.DataStore.save(avgSessionTime);
    print("Average session time of users: ${sum / st.length}");
  }
}
