// ignore_for_file: camel_case_types

import 'package:flutter/material.dart';
import 'package:amplify_flutter/amplify_flutter.dart';
import '../models/Tiempo.dart';
import '../models/User.dart';
import '../models/CategoryFrequency.dart';
import 'bottom_navigation_bar/bottom_nav_bar.dart';
import 'frequency.dart';
import 'goodFeed.dart';
import 'dart:async';
import 'package:connectivity_plus/connectivity_plus.dart';

class interest extends StatefulWidget {
  final String user;

  const interest({Key? key, required this.user}) : super(key: key);

  @override
  State<interest> createState() => _interestState();
}

class _interestState extends State<interest> {
  ConnectivityResult result = ConnectivityResult.none;
  late StreamSubscription subscription;
  List<bool> gustos = [false, false, false, false, false, false, false, false];
  List<Color> coloresBackground = [
    Colors.white,
    Colors.white,
    Colors.white,
    Colors.white,
    Colors.white,
    Colors.white,
    Colors.white,
    Colors.white,
  ];
  double getTimeNow() {
    String now = DateTime.now().toString();
    String time = now.split(" ")[1];
    var arraytime = time.split(":");
    int minute = int.parse(arraytime[1]);
    int hour = int.parse(arraytime[0]);
    double seconds = double.parse(arraytime[2]);
    double timeSeconds = minute * 60 + seconds + hour * 60 * 60;
    return timeSeconds;
  }

  double inicio = 0;
  @override
  void initState() {
    super.initState();
    inicio = getTimeNow();
    // registerFrequency("Interests");
    Connectivity().checkConnectivity().then((value) {
      this.result = value;
    });
    subscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      setState(() => this.result = result);
      if (result == ConnectivityResult.none) {
        setState(() => _showSnackBar());
      } else {
        setState(() => ScaffoldMessenger.of(context).hideCurrentSnackBar());
      }
      // Got a new connectivity status!
    });
  }

  dispose() {
    super.dispose();

    subscription.cancel();
  }

  void _showSnackBar() {
    ScaffoldMessenger.of(context).hideCurrentSnackBar();

    SnackBar snackBar = const SnackBar(
        content: Text("There is no internet connection"),
        duration: Duration(days: 1));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  List<Color> coloresText = [
    const Color.fromRGBO(0, 187, 169, 1),
    const Color.fromRGBO(0, 187, 169, 1),
    const Color.fromRGBO(0, 187, 169, 1),
    const Color.fromRGBO(0, 187, 169, 1),
    const Color.fromRGBO(0, 187, 169, 1),
    const Color.fromRGBO(0, 187, 169, 1),
    const Color.fromRGBO(0, 187, 169, 1),
    const Color.fromRGBO(0, 187, 169, 1),
  ];

  void changeColor(int i, bool click) {
    if (click) {
      setState(() {
        gustos[i] = click;
        coloresBackground[i] = const Color.fromRGBO(0, 187, 169, 1);
        coloresText[i] = Colors.white;
      });
    } else {
      setState(() {
        gustos[i] = click;
        coloresBackground[i] = Colors.white;
        coloresText[i] = const Color.fromRGBO(0, 187, 169, 1);
      });
    }
  }
  // ignore: slash_for_doc_comments
  /**MaterialStateProperty<Color> getColor(Color color, Color color2) {
    final getColor = (Set<MaterialState> states) {
      if (states.contains(MaterialState.pressed)) {
        return color2;
      } else {
        return color;
      }
    };
    return MaterialStateProperty.resolveWith(getColor);
  }**/

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
        backgroundColor: Colors.white,
        body: Form(
          child: SizedBox(
            width: size.width,
            height: size.height,
            child: Column(children: [
              const SizedBox(
                height: 40,
              ),
              Row(children: [
                RichText(
                    text: const TextSpan(
                        text: "   What are your ",
                        style: TextStyle(
                            color: Color.fromRGBO(55, 65, 64, 1),
                            fontWeight: FontWeight.w600,
                            fontSize: 20,
                            fontFamily: 'WorkSans'))),
                RichText(
                    text: const TextSpan(
                        text: "interests",
                        style: TextStyle(
                            color: Color.fromRGBO(0, 187, 169, 1),
                            fontWeight: FontWeight.w600,
                            fontSize: 20,
                            fontFamily: 'WorkSans'))),
                RichText(
                    text: const TextSpan(
                        text: "?",
                        style: TextStyle(
                            color: Color.fromRGBO(55, 65, 64, 1),
                            fontWeight: FontWeight.w400,
                            fontSize: 20,
                            fontFamily: 'WorkSans')))
              ]),
              const SizedBox(
                height: 40,
              ),
              Padding(
                padding: const EdgeInsets.all(5),
                child: Row(
                  children: [
                    MaterialButton(
                      elevation: 0.0,
                      child: Text('Movies, music & books',
                          style: TextStyle(
                              fontSize: 14,
                              fontFamily: "WorkSans",
                              fontWeight: FontWeight.w700,
                              color: coloresText[0])),
                      onPressed: () => gustos[0]
                          ? changeColor(0, false)
                          : changeColor(0, true),
                      color: coloresBackground[0],
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: const BorderSide(
                              width: 1.0,
                              color: Color.fromRGBO(0, 187, 169, 1))),
                      height: 40,
                    ),
                    const SizedBox(
                      width: 8,
                    ),
                    MaterialButton(
                      elevation: 0.0,
                      child: Text('Home',
                          style: TextStyle(
                              fontSize: 14,
                              fontFamily: "WorkSans",
                              fontWeight: FontWeight.w700,
                              color: coloresText[1])),
                      onPressed: () => gustos[1]
                          ? changeColor(1, false)
                          : changeColor(1, true),
                      color: coloresBackground[1],
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: const BorderSide(
                              width: 1.0,
                              color: Color.fromRGBO(0, 187, 169, 1))),
                      height: 40,
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(5),
                child: Row(
                  children: [
                    MaterialButton(
                      elevation: 0.0,
                      child: Text('Kitchen and dining',
                          style: TextStyle(
                              fontSize: 14,
                              fontFamily: "WorkSans",
                              fontWeight: FontWeight.w700,
                              color: coloresText[2])),
                      onPressed: () => gustos[2]
                          ? changeColor(2, false)
                          : changeColor(2, true),
                      color: coloresBackground[2],
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: const BorderSide(
                              width: 1.0,
                              color: Color.fromRGBO(0, 187, 169, 1))),
                      height: 40,
                    ),
                    const SizedBox(
                      width: 8,
                    ),
                    MaterialButton(
                      elevation: 0.0,
                      child: Text('Musical Instruments',
                          style: TextStyle(
                              fontSize: 14,
                              fontFamily: "WorkSans",
                              fontWeight: FontWeight.w700,
                              color: coloresText[3])),
                      onPressed: () => gustos[3]
                          ? changeColor(3, false)
                          : changeColor(3, true),
                      color: coloresBackground[3],
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: const BorderSide(
                              width: 1.0,
                              color: Color.fromRGBO(0, 187, 169, 1))),
                      height: 40,
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(5),
                child: Row(
                  children: [
                    MaterialButton(
                      elevation: 0.0,
                      child: Text('Garden',
                          style: TextStyle(
                              fontSize: 14,
                              fontFamily: "WorkSans",
                              fontWeight: FontWeight.w700,
                              color: coloresText[4])),
                      onPressed: () => gustos[4]
                          ? changeColor(4, false)
                          : changeColor(4, true),
                      color: coloresBackground[4],
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: const BorderSide(
                              width: 1.0,
                              color: Color.fromRGBO(0, 187, 169, 1))),
                      height: 40,
                    ),
                    const SizedBox(
                      width: 8,
                    ),
                    MaterialButton(
                      elevation: 0.0,
                      child: Text('School Office Supplies',
                          style: TextStyle(
                              fontSize: 14,
                              fontFamily: "WorkSans",
                              fontWeight: FontWeight.w700,
                              color: coloresText[5])),
                      onPressed: () => gustos[5]
                          ? changeColor(5, false)
                          : changeColor(5, true),
                      color: coloresBackground[5],
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: const BorderSide(
                              width: 1.0,
                              color: Color.fromRGBO(0, 187, 169, 1))),
                      height: 40,
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(5),
                child: Row(
                  children: [
                    MaterialButton(
                      elevation: 0.0,
                      child: Text('Toys',
                          style: TextStyle(
                              fontSize: 14,
                              fontFamily: "WorkSans",
                              fontWeight: FontWeight.w700,
                              color: coloresText[6])),
                      onPressed: () => gustos[6]
                          ? changeColor(6, false)
                          : changeColor(6, true),
                      color: coloresBackground[6],
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: const BorderSide(
                              width: 1.0,
                              color: Color.fromRGBO(0, 187, 169, 1))),
                      height: 40,
                    ),
                    const SizedBox(
                      width: 8,
                    ),
                    MaterialButton(
                      elevation: 0.0,
                      child: Text('Women',
                          style: TextStyle(
                              fontSize: 14,
                              fontFamily: "WorkSans",
                              fontWeight: FontWeight.w700,
                              color: coloresText[7])),
                      onPressed: () => gustos[7]
                          ? changeColor(7, false)
                          : changeColor(7, true),
                      color: coloresBackground[7],
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: const BorderSide(
                              width: 1.0,
                              color: Color.fromRGBO(0, 187, 169, 1))),
                      height: 40,
                    ),
                  ],
                ),
              ),
              /*  ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: getColor(Colors.white, Colors.red),
                ),
                child: Text("change color"),
                onPressed: () {},
              ),*/
              const SizedBox(
                height: 160,
              ),
              MaterialButton(
                child: const Text('Continue',
                    style: TextStyle(
                        fontSize: 14,
                        fontFamily: "WorkSans",
                        fontWeight: FontWeight.w700,
                        color: Color.fromRGBO(255, 255, 255, 1))),
                onPressed: () async {
                  double last = getTimeNow();
                  double tiempoStay = last - inicio;
                  try {
                    final tiemporegister =
                        Tiempo(name: "Interest", time: tiempoStay);
                    await Amplify.DataStore.save(tiemporegister);
                  } catch (e) {
                    print(e);
                  }
                  _saveInterests(context);
                },
                color: const Color.fromRGBO(246, 65, 108, 1),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0)),
                minWidth: size.width - 80.0,
                height: 40,
              ),
            ]),
          ),
        ));
  }

  Future<List<String>> checkInterest() async {
    List<String> categories = [];
    List<String> categorias = [
      "Movies, music & books",
      "Home",
      "Kitchen & Dining",
      "Musical instruments",
      "Garden",
      "School office supplies",
      "Toys",
      "Women"
    ];
    for (int i = 0; i < gustos.length; i++) {
      if (gustos[i] == true) {
        categories.add(categorias[i]);
        final CategoryObject = await Amplify.DataStore.query(
            CategoryFrequency.classType,
            where: CategoryFrequency.CATEGORY.eq(categorias[i]));
        var numeroSelected = CategoryObject[0].selected;
        var categ = CategoryObject[0].copyWith(selected: numeroSelected + 1);
        await Amplify.DataStore.save(categ);
      }
    }
    return categories;
  }

  void _saveInterests(BuildContext context) async {
    List<String> categories = await checkInterest();
    // print("revisar");
    // print(categories);
    try {
      String userr = widget.user;

      // print(email);
      // print(userr.username);
      final UserObject = await Amplify.DataStore.query(User.classType,
          where: User.USERNAME.eq(userr));

      final UpdateObject = UserObject[0].copyWith(interests: categories);

      await Amplify.DataStore.save(UpdateObject);
      // print("se logro");
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (_) => BottomNavBar(),
        ),
      );
      registerFrequency("Feed");
    } catch (e) {
      // print(e);
    }
  }
}
