// ignore_for_file: non_constant_identifier_names, file_names

import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
// ignore: library_prefixes
import 'package:flutter/services.dart' as rootBundle;
import 'package:gifter/screens/listStores.dart';
import 'package:gifter/screens/profile.dart';
import './StoreDataModel.dart';
import 'giftFeed.dart';
import './profile.dart';

class GoodFeed extends StatefulWidget {
  const GoodFeed({Key? key}) : super(key: key);

  @override
  State<GoodFeed> createState() => _GoodFeedState();
}

class _GoodFeedState extends State<GoodFeed> {
  late int _selectedIndex;

  late List<Widget> _pages;

  double getTimeNow() {
    String now = DateTime.now().toString();
    String time = now.split(" ")[1];
    var arraytime = time.split(":");
    int minute = int.parse(arraytime[1]);
    int hour = int.parse(arraytime[0]);
    double seconds = double.parse(arraytime[2]);
    double timeSeconds = minute * 60 + seconds + hour * 60 * 60;
    return timeSeconds;
  }

  var fetch;

  double inicio = 0;

  void initState() {
    super.initState();
    // registerFrequency("Feed");
  }

  Widget FallbackView() {
    return Container(
      child: Text("No internet connection, try again later"),
    );
  }

  Future<List> ReadJsonData() async {
    //read json file
    final jsondata =
        await rootBundle.rootBundle.loadString('../assets/stores.json');
    //decode json data as list
    final list = json.decode(jsondata) as List<dynamic>;

    //map json and initialize using DataModel
    return list.map((e) => StoreDataModel.fromJson(e)).toList();
  }

  @override
  Widget build(BuildContext context) {
    // final Size size = MediaQuery.of(context).size;
    _selectedIndex = 0;
    _pages = <Widget>[
      const GoodFeed(),
      const ListStores(),
      const GoodFeed(),
      profile(context, donde: 0),
      const GoodFeed()
    ];

    return SafeArea(
      child: Scaffold(
        body: Column(
          children: [
            GifterLogo,
            // GiftShopsText,
            // StoresList,
            // const Padding(padding: EdgeInsets.only(bottom: 16)),
            // GetDivider,
            PopularGiftsText,
            GiftFeed(),
            //GiftsList
          ],
        ),
        // bottomNavigationBar: buildNavigation(),
      ),
    );
  }

  Widget get GifterLogo {
    return Container(
        padding: const EdgeInsets.only(left: 16, top: 16, bottom: 16),
        child: Row(
          // ignore: prefer_const_literals_to_create_immutables
          children: [
            const Image(
              image: AssetImage("assets/logo.png"),
              height: 27,
            ),
            const Padding(padding: EdgeInsets.only(right: 4)),
            const Text('Gifter',
                style: TextStyle(
                    fontSize: 18,
                    fontFamily: 'PTSerif',
                    fontWeight: FontWeight.w700,
                    color: Color.fromRGBO(23, 27, 26, 1)))
          ],
        ));
  }

  Widget get GiftShopsText {
    return Container(
        padding: const EdgeInsets.only(left: 16, bottom: 16),
        // ignore: prefer_const_literals_to_create_immutables
        child: Row(children: [
          const Text('Take a look at gift shops near you',
              style: TextStyle(
                  fontSize: 16,
                  fontFamily: "Montserrat",
                  fontWeight: FontWeight.w600,
                  color: Color.fromRGBO(23, 27, 26, 1)))
        ]));
  }

  // Widget get StoresList {
  //   return const SizedBox(height: 180, child: StoresView());
  // }

  Widget get GetDivider {
    return const Divider(
      color: Color.fromARGB(244, 230, 230, 230),
      thickness: 8,
      height: 24,
    );
  }

  Widget get PopularGiftsText {
    return Container(
        padding: const EdgeInsets.only(left: 16, top: 8, bottom: 16),
        // ignore: prefer_const_literals_to_create_immutables
        child: Row(children: [
          const Text('Popular gift ideas',
              style: TextStyle(
                  fontSize: 16,
                  fontFamily: "Montserrat",
                  fontWeight: FontWeight.w600,
                  color: Color.fromRGBO(23, 27, 26, 1)))
        ]));
  }

  // Widget buildNavigation() {
  //   return BottomNavigationBar(
  //     selectedItemColor: const Color.fromRGBO(246, 65, 108, 1),
  //     unselectedItemColor: const Color.fromRGBO(196, 196, 196, 1),
  //     currentIndex: _selectedIndex,
  //     // ignore: prefer_const_literals_to_create_immutables
  //     items: [
  //       const BottomNavigationBarItem(
  //         icon: Icon(Icons.home),
  //         label: 'Feed',
  //       ),
  //       const BottomNavigationBarItem(
  //         icon: Icon(Icons.storefront),
  //         label: 'Stores',
  //       ),
  //       const BottomNavigationBarItem(
  //         icon: Icon(Icons.search),
  //         label: 'Search',
  //       ),
  //       const BottomNavigationBarItem(
  //         icon: Icon(Icons.person),
  //         label: 'Profile',
  //       ),
  //       const BottomNavigationBarItem(
  //         icon: Icon(Icons.menu),
  //         label: 'Menu',
  //       ),
  //     ],
  //     onTap: _onItemTapped,
  //   );
  // }

  // void _onItemTapped(int index) {
  //   setState(() {
  //     _selectedIndex = index;
  //   });
  //   Navigator.push(
  //     context,
  //     MaterialPageRoute(builder: (_) => _pages.elementAt(_selectedIndex)),
  //   );
  // }
}
