// ignore_for_file: camel_case_types, deprecated_member_use

import 'package:amplify_api/model_queries.dart';
import 'package:amplify_auth_cognito/amplify_auth_cognito.dart';
import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:flutter/material.dart';
import 'package:gifter/screens/bottom_navigation_bar/bottom_nav_bar.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:gifter/screens/dialogs.dart';
import 'package:gifter/screens/friendProfile.dart';
import 'package:gifter/screens/profile.dart';
import 'package:gifter/screens/search.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../models/Tiempo.dart';
import '../models/User.dart';
import './signup.dart';
import './email.dart';
import 'dart:async';

import 'averageSessionTime.dart';
import 'frequency.dart';

class login extends StatefulWidget {
  login({Key? key}) : super(key: key);

  @override
  State<login> createState() => _loginState();
}

class _loginState extends State<login> {
  ConnectivityResult result = ConnectivityResult.none;
  late StreamSubscription subscription;

  final GlobalKey<State> _keyLoader = GlobalKey<State>();

  final TextEditingController _username = TextEditingController();

  final TextEditingController _password = TextEditingController();
  double getTimeNow() {
    String now = DateTime.now().toString();
    String time = now.split(" ")[1];
    var arraytime = time.split(":");
    int minute = int.parse(arraytime[1]);
    int hour = int.parse(arraytime[0]);
    double seconds = double.parse(arraytime[2]);
    double timeSeconds = minute * 60 + seconds + hour * 60 * 60;
    return timeSeconds;
  }

  double inicio = 0;
  // ignore: unused_field
  final TextEditingController _code = TextEditingController();
  @override
  void initState() {
    super.initState();
    inicio = getTimeNow();
    // registerFrequency("Login");
    Connectivity().checkConnectivity().then((value) {
      this.result = value;
    });
    subscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      setState(() => this.result = result);
      if (result == ConnectivityResult.none) {
        setState(() => _showSnackBar());
      } else {
        setState(() => ScaffoldMessenger.of(context).hideCurrentSnackBar());
      }
      // Got a new connectivity status!
    });
  }

  dispose() {
    super.dispose();

    subscription.cancel();
  }

  void _showSnackBar() {
    ScaffoldMessenger.of(context).hideCurrentSnackBar();

    SnackBar snackBar = const SnackBar(
        content: Text("There is no internet connection"),
        duration: Duration(days: 1));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.transparent,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back,
              color: Color.fromRGBO(246, 65, 108, 1)),
          onPressed: () async {
            double last = getTimeNow();
            double tiempoStay = last - inicio;
            try {
              final tiemporegister = Tiempo(name: "Login", time: tiempoStay);
              await Amplify.DataStore.save(tiemporegister);
            } catch (e) {}
            Navigator.of(context).pop();
            registerFrequency("First Screen");
          },
        ),
        title: RichText(
            text: const TextSpan(
                text: "Log In",
                style: TextStyle(
                    color: Color.fromRGBO(246, 65, 108, 1),
                    fontWeight: FontWeight.w600,
                    fontSize: 17,
                    fontFamily: 'WorkSans'))),
        elevation: 0.0,
      ),
      body: SingleChildScrollView(
        child: SizedBox(
          width: size.width,
          height: size.height,
          child: Column(
            children: [
              SizedBox(
                height: size.height * (1 / 14.8),
              ),
              Image(
                image: const AssetImage("assets/logo.png"),
                height: size.height * 0.1,
              ),
              SizedBox(
                height: size.height * (1 / 12.333),
              ),
              SizedBox(
                width: size.width - 50,
                height: size.height * (1 / 16.44),
                child: Material(
                  elevation: 3.0,
                  shadowColor: Colors.grey,
                  child: TextFormField(
                    autofocus: false,
                    controller: _username,
                    decoration: InputDecoration(
                        hintText: 'Username',
                        fillColor: Colors.white,
                        filled: true,
                        contentPadding:
                            const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20.0),
                            borderSide: const BorderSide(
                                color: Color.fromRGBO(255, 255, 255, 150)))),
                  ),
                  borderRadius: BorderRadius.circular(20.0),
                ),
              ),
              SizedBox(
                height: size.height * (1 / 37),
              ),
              SizedBox(
                width: size.width - 50,
                height: size.height * (1 / 16.44),
                child: Material(
                  elevation: 3.0,
                  shadowColor: Colors.grey,
                  child: TextFormField(
                    obscureText: true,
                    autofocus: false,
                    controller: _password,
                    decoration: InputDecoration(
                        hintText: 'Password',
                        fillColor: Colors.white,
                        filled: true,
                        contentPadding:
                            const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20.0),
                            borderSide: const BorderSide(
                                color: Color.fromRGBO(255, 255, 255, 150)))),
                  ),
                  borderRadius: BorderRadius.circular(20.0),
                ),
              ),
              SizedBox(
                height: size.height * (1 / 12.333),
              ),
              MaterialButton(
                child: const Text('Log in',
                    style: TextStyle(
                        fontSize: 14,
                        fontFamily: "WorkSans",
                        fontWeight: FontWeight.w700,
                        color: Color.fromRGBO(255, 255, 255, 1))),
                onPressed: () async {
                  double last = getTimeNow();
                  double tiempoStay = last - inicio;
                  try {
                    final tiemporegister =
                        Tiempo(name: "Login", time: tiempoStay);
                    await Amplify.DataStore.save(tiemporegister);
                  } catch (e) {
                    print(e);
                  }
                  _verifyLogin(context);
                },
                color: const Color.fromRGBO(246, 65, 108, 1),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0)),
                minWidth: size.width - 80.0,
                height: 40,
              ),
              SizedBox(
                height: size.height * (1 / 74),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _verifyLogin(BuildContext context) async {
    final prefs = await SharedPreferences.getInstance();
    final online = prefs.getString('online') ?? "error user";
    try {
      if (result == ConnectivityResult.none) {
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
              title: const Text("There is no network connection"),
              content: const Text(
                  "Please check your network connection and try again"),
              actions: <Widget>[
                FlatButton(
                  child: const Text("Continue"),
                  onPressed: Navigator.of(context).pop,
                )
              ]),
        );
        return;
      }
      print("3");
      final UserObject = await Amplify.DataStore.query(User.classType);

      // ignore: unused_local_variable
      print(online);
      if (online == "1") {
        await Amplify.Auth.signOut();
        prefs.setString("online", "0");
      }
      print("4");
      //send user to dashboard
    } on AuthException catch (e) {
      //

    }
    final user = _username.text;
    final password = _password.text;

    if (user != "" && password != "") {
      Dialogs.showLoadingDialog(context, _keyLoader);
      print("2");
      try {
        // ignore: non_constant_identifier_names

        var Signin =
            await Amplify.Auth.signIn(username: user, password: password);
        if (Signin.isSignedIn) {
          // print("yay");
          Navigator.of(_keyLoader.currentContext!, rootNavigator: true).pop();
          prefs.setString('user', user);

          var mirando = prefs.getString('user');
          prefs.setString("online", "1");
          var comenzar = getTimeNow();
          prefs.setDouble("comenzar", comenzar);
          prefs.setString("conteo", "0");
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (_) => BottomNavBar(),
            ),
          );
          registerFrequency("Feed");
          setStart(getTimeNow());
        } else {}
      } on AuthException catch (e) {
        if (e.runtimeType == UserNotFoundException) {
          Navigator.of(_keyLoader.currentContext!, rootNavigator: true).pop();
          showDialog(
            context: context,
            builder: (context) => AlertDialog(
                title: const Text("Invalid username or password"),
                content: const Text("Please try again"),
                actions: <Widget>[
                  FlatButton(
                    child: const Text("Continue"),
                    onPressed: Navigator.of(context).pop,
                  )
                ]),
          );
        } else if (e.runtimeType == UserNotConfirmedException) {
          String word = "";

          final request = ModelQueries.list(User.classType);
          print("viendo1");
          final response = await Amplify.API.query(request: request).response;
          var resultado = response.data?.items;
          // print(resultado);
          // print("viendo2");
          // ignore: unused_local_variable
          bool verificar = false;
          for (int i = 0; i < resultado!.length; i++) {
            if (resultado[i]?.username == user) {
              word = resultado[i]!.email;
            }
          }
          Navigator.of(_keyLoader.currentContext!, rootNavigator: true).pop();
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (_) =>
                  emailpage(emails: word, users: user, password: password),
            ),
          );
          registerFrequency("Email Confirmation");
        } else if (e.runtimeType == NotAuthorizedException) {
          Navigator.of(_keyLoader.currentContext!, rootNavigator: true).pop();
          showDialog(
            context: context,
            builder: (context) => AlertDialog(
                title: const Text("Username and password not match"),
                content: const Text("please try again"),
                actions: <Widget>[
                  FlatButton(
                    child: const Text("Continue"),
                    onPressed: Navigator.of(context).pop,
                  )
                ]),
          );
        } else {
          Navigator.of(_keyLoader.currentContext!, rootNavigator: true).pop();
          showDialog(
            context: context,
            builder: (context) => AlertDialog(
                title: const Text("There was an error"),
                content: const Text("Check network connectivity"),
                actions: <Widget>[
                  FlatButton(
                    child: const Text("Continue"),
                    onPressed: Navigator.of(context).pop,
                  )
                ]),
          );
          Navigator.of(_keyLoader.currentContext!, rootNavigator: true).pop();
        }
      }
    } else {
      print("5");
      // Navigator.of(_keyLoader.currentContext!, rootNavigator: true).pop();
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
            title: const Text("Please write the username and password"),
            content: const Text("All fields are required"),
            actions: <Widget>[
              FlatButton(
                child: const Text("Continue"),
                onPressed: Navigator.of(context).pop,
              )
            ]),
      );
    }
  }
}
