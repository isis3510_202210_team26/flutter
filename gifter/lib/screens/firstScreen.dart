// ignore: file_names
import 'dart:async';

import 'package:amplify_auth_cognito/amplify_auth_cognito.dart';
import 'package:amplify_flutter/amplify_flutter.dart';
import "package:flutter/material.dart";
import 'package:gifter/screens/dialogs.dart';
import '../models/Tiempo.dart';
import '../models/Time.dart';
import "./login.dart";
import "./signup.dart";
import 'package:connectivity_plus/connectivity_plus.dart';

import 'frequency.dart';

// ignore: camel_case_types
class firstScreen extends StatefulWidget {
  firstScreen({Key? key}) : super(key: key);

  @override
  State<firstScreen> createState() => _firstScreenState();
}

class _firstScreenState extends State<firstScreen> {
  final GlobalKey<State> _keyLoader = GlobalKey<State>();

  ConnectivityResult result = ConnectivityResult.none;
  late StreamSubscription subscription;
  double getTimeNow() {
    String now = DateTime.now().toString();
    String time = now.split(" ")[1];
    var arraytime = time.split(":");
    int minute = int.parse(arraytime[1]);
    int hour = int.parse(arraytime[0]);
    double seconds = double.parse(arraytime[2]);
    double timeSeconds = minute * 60 + seconds + hour * 60 * 60;
    return timeSeconds;
  }

  double inicio = 0;
  var tiemporev;
  @override
  void initState() {
    super.initState();
    inicio = getTimeNow();
    // registerFrequency("First Screen");
    print(inicio);
    tiemporev = Time(name: "firstScreen", time: inicio);
    var revisar;
    Connectivity().checkConnectivity().then((value) {
      if (value == ConnectivityResult.none) {
        _showSnackBar();
      }
    });
    subscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      setState(() => this.result = result);
      if (result == ConnectivityResult.none) {
        setState(() => _showSnackBar());
      } else {
        setState(() => ScaffoldMessenger.of(context).hideCurrentSnackBar());
      }
      // Got a new connectivity status!
    });
  }

  dispose() {
    super.dispose();

    subscription.cancel();
  }

  void _showSnackBar() {
    ScaffoldMessenger.of(context).hideCurrentSnackBar();

    SnackBar snackBar = const SnackBar(
        content: Text("There is no internet connection"),
        duration: Duration(days: 1));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Colors.white,
      body: SizedBox(
        width: size.width,
        height: size.height,
        child: Column(
          children: [
            Image(
              image: const AssetImage("assets/app_header.png"),
              width: size.width,
            ),
            Padding(
              padding: EdgeInsets.all(size.height * (1 / 18)),
              child: Column(children: [
                Image(
                  image: const AssetImage("assets/logo.png"),
                  height: size.height * 0.1,
                ),
                SizedBox(
                  height: size.height * (1 / 18.5),
                ),
                RichText(
                    text: const TextSpan(
                        text: "Gifter",
                        style: TextStyle(
                            color: Color.fromRGBO(0, 0, 0, 1),
                            fontWeight: FontWeight.w700,
                            fontSize: 35,
                            fontFamily: 'WorkSans'))),
                SizedBox(
                  height: size.height * (1 / 74),
                ),
                RichText(
                    text: const TextSpan(
                        text: "Find the perfect gift... always",
                        style: TextStyle(
                            color: Color.fromRGBO(0, 0, 0, 1),
                            fontWeight: FontWeight.w400,
                            fontSize: 15,
                            fontFamily: 'WorkSans'))),
                SizedBox(
                  height: size.height * (1 / 18.5),
                ),
                MaterialButton(
                  child: const Text('Sign up',
                      style: TextStyle(
                          fontSize: 14,
                          fontFamily: "WorkSans",
                          fontWeight: FontWeight.w700,
                          color: Color.fromRGBO(255, 255, 255, 1))),
                  onPressed: () async {
                    double last = getTimeNow();
                    var inicio2 = tiemporev.time;

                    double tiempoStay = last - inicio2;
                    try {
                      final tiemporegister =
                          Tiempo(name: "First Screen", time: tiempoStay);
                      await Amplify.DataStore.save(tiemporegister);
                    } catch (e) {
                      print(e);
                    }
                    _gotoSignUpScreen(context);
                  },
                  color: const Color.fromRGBO(246, 65, 108, 1),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0)),
                  minWidth: size.width - 80.0,
                  height: size.height * (1 / 18.5),
                ),
                SizedBox(
                  height: size.height * (1 / 72),
                ),
                GestureDetector(
                  onTap: () async {
                    double last = getTimeNow();
                    double tiempoStay = last - inicio;
                    try {
                      final tiemporegister =
                          Tiempo(name: "First Screen", time: tiempoStay);
                      await Amplify.DataStore.save(tiemporegister);
                    } catch (e) {
                      print(e);
                    }
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (_) => login(),
                      ),
                    );
                    registerFrequency("Login");
                  },
                  child: RichText(
                      text: const TextSpan(
                          text: "Login",
                          style: TextStyle(
                              color: Color.fromRGBO(0, 0, 0, 1),
                              fontWeight: FontWeight.w600,
                              fontSize: 15))),
                ),
              ]),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _gotoSignUpScreen(BuildContext context) async {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (_) => signup(),
      ),
    );
    registerFrequency("Sign Up");
  }
}
