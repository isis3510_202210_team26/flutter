import 'dart:async';

import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:connectivity_plus/connectivity_plus.dart';

class connectivity extends StatefulWidget {
  connectivity({Key? key}) : super(key: key);

  @override
  State<connectivity> createState() => _connectivityState();
}

class _connectivityState extends State<connectivity> {
  ConnectivityResult result = ConnectivityResult.none;
  late StreamSubscription subscription;

  @override
  void initState() {
    super.initState();

    subscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      setState(() => this.result = result);
      if (result == ConnectivityResult.none) {
        setState(() => _showSnackBar());
      } else {
        setState(() => ScaffoldMessenger.of(context).hideCurrentSnackBar());
      }
      // Got a new connectivity status!
    });
  }

  dispose() {
    super.dispose();

    subscription.cancel();
  }

  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: ElevatedButton(
        child: Text(result.toString()),
        onPressed: () {
          setState(() {
            _showSnackBar();
          });
        },
      ),
    ));
  }

  void _showSnackBar() {
    ScaffoldMessenger.of(context).hideCurrentSnackBar();

    SnackBar snackBar =
        SnackBar(content: Text(result.toString()), duration: Duration(days: 1));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }
}
