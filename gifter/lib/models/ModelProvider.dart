/*
* Copyright 2021 Amazon.com, Inc. or its affiliates. All Rights Reserved.
*
* Licensed under the Apache License, Version 2.0 (the "License").
* You may not use this file except in compliance with the License.
* A copy of the License is located at
*
*  http://aws.amazon.com/apache2.0
*
* or in the "license" file accompanying this file. This file is distributed
* on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied. See the License for the specific language governing
* permissions and limitations under the License.
*/

// NOTE: This file is generated and may not follow lint rules defined in your app
// Generated files can be excluded from analysis in analysis_options.yaml
// For more info, see: https://dart.dev/guides/language/analysis-options#excluding-code-from-analysis

// ignore_for_file: public_member_api_docs, file_names, unnecessary_new, prefer_if_null_operators, prefer_const_constructors, slash_for_doc_comments, annotate_overrides, non_constant_identifier_names, unnecessary_string_interpolations, prefer_adjacent_string_concatenation, unnecessary_const, dead_code

import 'package:amplify_core/amplify_core.dart';
import 'AvgSessionTime.dart';
import 'CategoryFrequency.dart';
import 'FavoriteStores.dart';
import 'Gift.dart';
import 'HomeToProfile.dart';
import 'SectionsFrequency.dart';
import 'Tiempo.dart';
import 'User.dart';

export 'AvgSessionTime.dart';
export 'CategoryFrequency.dart';
export 'FavoriteStores.dart';
export 'Gift.dart';
export 'HomeToProfile.dart';
export 'SectionsFrequency.dart';
export 'Tiempo.dart';
export 'User.dart';

class ModelProvider implements ModelProviderInterface {
  @override
  String version = "d44a9ee7fe4c9e58babbdb120d1116f3";
  @override
  List<ModelSchema> modelSchemas = [AvgSessionTime.schema, CategoryFrequency.schema, FavoriteStores.schema, Gift.schema, HomeToProfile.schema, SectionsFrequency.schema, Tiempo.schema, User.schema];
  static final ModelProvider _instance = ModelProvider();
  @override
  List<ModelSchema> customTypeSchemas = [];

  static ModelProvider get instance => _instance;
  
  ModelType getModelTypeByModelName(String modelName) {
    switch(modelName) {
      case "AvgSessionTime":
        return AvgSessionTime.classType;
      case "CategoryFrequency":
        return CategoryFrequency.classType;
      case "FavoriteStores":
        return FavoriteStores.classType;
      case "Gift":
        return Gift.classType;
      case "HomeToProfile":
        return HomeToProfile.classType;
      case "SectionsFrequency":
        return SectionsFrequency.classType;
      case "Tiempo":
        return Tiempo.classType;
      case "User":
        return User.classType;
      default:
        throw Exception("Failed to find model in model provider for model name: " + modelName);
    }
  }
}