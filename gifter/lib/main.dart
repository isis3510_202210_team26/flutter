import 'package:amplify_auth_cognito/amplify_auth_cognito.dart';
import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:amplify_storage_s3/amplify_storage_s3.dart';
import 'package:flutter/material.dart';
import 'package:gifter/amplifyconfiguration.dart';
import 'package:amplify_api/amplify_api.dart';
import 'package:gifter/screens/bottom_navigation_bar/bottom_nav_bar.dart';
import 'package:gifter/screens/dismissKeyboard.dart';
import 'package:gifter/screens/firstScreen.dart';
import 'package:gifter/screens/giftFeed.dart';
import 'package:gifter/screens/goodFeed.dart';
import 'package:gifter/screens/profile.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'models/ModelProvider.dart';
import 'package:amplify_datastore/amplify_datastore.dart';
import 'amplifyconfiguration.dart';
import 'package:flutter/services.dart';

void main() {
  // Disabling horizontal orientation for the app
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);

  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool _isAmplifyConfigured = true;
  @override
  void initState() {
    super.initState();
    WebView.platform = AndroidWebView();

    _configureAmplify();
  }

  Future<void> _configureAmplify() async {
    try {
      // final provider = ModelProvider();

      // final dataStorePlugin = AmplifyDataStore(modelProvider: provider);

      // Add the following line to add Auth plugin to your app.

      // await Amplify.addPlugin(dataStorePlugin);
      //await Amplify.configure(amplifyconfig);
      // print("b");
      await Amplify.addPlugins([
        AmplifyAuthCognito(),
        AmplifyDataStore(modelProvider: ModelProvider.instance),
        AmplifyStorageS3(),
        AmplifyAPI(modelProvider: ModelProvider.instance)
      ]);

      // call Amplify.configure to use the initialized categories in your app
      await Amplify.configure(amplifyconfig);
      // print("c");
    } on Exception catch (e) {
      // print('An error occurred configuring Amplify: $e');
    }
    setState(() {
      _isAmplifyConfigured = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    // double screenWidth = window.physicalSize.width;

    return DismissKeyboard(
        child: MaterialApp(
            home: _isAmplifyConfigured
                ? const Center(child: CircularProgressIndicator())
                : firstScreen()));
  }
}
